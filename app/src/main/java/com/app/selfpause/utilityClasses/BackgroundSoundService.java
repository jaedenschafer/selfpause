package com.app.selfpause.utilityClasses;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.app.selfpause.JavaFragment.LibraryFragmentNew;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.AllCatAndRecomendedActivity;
import com.app.selfpause.javaActivities.CreativityAffirmationActivityNew;

import java.io.IOException;
import java.util.ArrayList;

import static com.app.selfpause.javaActivities.CreativityAffirmationActivityNew.CHANNEL_ID;
import static com.app.selfpause.javaActivities.CreativityAffirmationActivityNew.dialog;
import static com.app.selfpause.javaActivities.CreativityAffirmationActivityNew.seekbarUpdateHandler;
import static com.app.selfpause.javaActivities.CreativityAffirmationActivityNew.updateSeekBar;

public class BackgroundSoundService extends Service {

    private static final String TAG = "BackgroundSoundService";
    MediaPlayer mediaPlayer;
    String song, time;
    ArrayList<String> playlist;
    Boolean start = false;
    int i = 1;
    Notification notification;
    //    int durationTimer;
    private boolean isPrepared;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("player", "onBind()");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
        Log.e("playerM", "onCreate() , service started...");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {

            if (intent.getStringExtra("player").equals("Play")) {

                song = intent.getStringExtra("main_song");
                playlist = intent.getStringArrayListExtra("playlist");

                notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle(intent.getStringExtra("song_name"))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setSound(null)
                        .build();
                startForeground(1, notification);

                notification.flags = Notification.FLAG_ONGOING_EVENT;
                CreativityAffirmationActivityNew.back = false;
//                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                durationTimer = intent.getIntExtra("duration", 0);
//                time = milliSecondsToTimer(durationTimer);
                if (song != null) {
                    try {
                        if (mediaPlayer != null) {
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }
                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//                        mediaPlayer = MediaPlayer.create(this, R.raw.a);
//                        sendInfoBroadcast();
//                        mediaPlayer.start();
                        mediaPlayer.setDataSource(song);
                        mediaPlayer.prepareAsync();
                        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                start = true;
                                CreativityAffirmationActivityNew.start();
                                mediaPlayer.start();
//                                seekbarUpdateHandler.postDelayed(updateSeekBar,500);
//                                dialog.dismiss();
                                Log.e("current", String.valueOf(mediaPlayer.getCurrentPosition()));
                                sendInfoBroadcast();
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            if (start) {
                                if (playlist != null) {
                                    if (playlist.size() > i) {
                                        CreativityAffirmationActivityNew.pause();
                                        Intent m_intent = new Intent(BackgroundSoundService.this, BackgroundSoundService.class);
                                        m_intent.putExtra("main_song", playlist.get(i));
                                        Log.e(String.valueOf(i), playlist.get(i));
                                        m_intent.putStringArrayListExtra("playlist", playlist);
                                        m_intent.putExtra("player", "Play");
                                        ContextCompat.startForegroundService(BackgroundSoundService.this, m_intent);
                                        start = false;

//                                    try {
//                                        mediaPlayer.setDataSource(playlist.get(i + 1));
//                                        mediaPlayer.prepareAsync();
//                                        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                                            @Override
//                                            public void onPrepared(MediaPlayer mp) {
//                                                mediaPlayer.start();
//                                                sendInfoBroadcast();
//                                            }
//                                        });
//                                    } catch (IOException e) {
//                                        e.printStackTrace();
//                                    }
                                        i++;
                                    } else {
                                        CreativityAffirmationActivityNew.pause();
                                        if (isMyServiceRunning(NatureSoundService.class)) {
                                            stopService(new Intent(BackgroundSoundService.this, NatureSoundService.class));
                                        }
                                        start = false;
                                    }
                                } else {
                                    CreativityAffirmationActivityNew.pause();
                                    if (isMyServiceRunning(NatureSoundService.class)) {
                                        stopService(new Intent(BackgroundSoundService.this, NatureSoundService.class));
                                    }
                                    start = false;
                                }
                            } else {
                                CreativityAffirmationActivityNew.pause();
                                if (isMyServiceRunning(NatureSoundService.class)) {
                                    stopService(new Intent(BackgroundSoundService.this, NatureSoundService.class));
                                }
                            }
                        }
                    });
                }
            }
//
            if (intent.getStringExtra("player").equals("Pause")) {
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        pause();
                    }
                }
            }
            //
            if (intent.getStringExtra("player").equals("Resume")) {
                if (mediaPlayer != null) {
                    if (!mediaPlayer.isPlaying()) {
                        play();
                    }
                }
            }
            //
//            if (intent.getStringExtra("player").equals("Change")) {
//                song = intent.getStringExtra("main_song");
//                if (song != null) {
//                    try {
//                        if (mediaPlayer != null) {
//                            mediaPlayer.release();
//                            mediaPlayer = null;
//                        }
//                        mediaPlayer = new MediaPlayer();
//                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//                        mediaPlayer.setDataSource(song);
//                        mediaPlayer.prepareAsync();
//                        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                            @Override
//                            public void onPrepared(MediaPlayer mp) {
//                                mediaPlayer.start();
//                                sendInfoBroadcast();
//                                Log.e("player", "play");
//                            }
//                        });
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
            //
            if (intent.getStringExtra("player").equals("ACTION_SEND_INFO")) {
                sendInfoBroadcast();

            }
            //
            if (intent.getStringExtra("player").equals("ACTION_SEEK_TO")) {
                int time = intent.getIntExtra("seek_to", 0);
                seekTo(time);
            }

            if (intent.getStringExtra("player").equals("Seek_backward")) {
                if (mediaPlayer != null) {
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 5000);
                }
            }

            if (intent.getStringExtra("player").equals("Seek_forward")) {
                if (mediaPlayer != null) {
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 5000);
                }
            }

            if (intent.getStringExtra("player").equals("Stop_notification")) {
                stopNotification_Background();
            }

        }
        return Service.START_NOT_STICKY;
    }

    public IBinder onUnBind(Intent arg0) {
        Log.e("player", "onUnBind()");
        return null;
    }


    @Override
    public void onDestroy() {
        LibraryFragmentNew.update = true;
        if (isMyServiceRunning(NatureSoundService.class)) {
            stopService(new Intent(BackgroundSoundService.this, NatureSoundService.class));
        }
        stop();
        release();
//        seekbarUpdateHandler.removeCallbacks(updateSeekBar);
//        Toast.makeText(this, "Service stoppedM...", Toast.LENGTH_SHORT).show();
        Log.e("player", "onCreate() , service stopped...");
    }

    @Override
    public void onLowMemory() {
        Log.e("player", "onLowMemory()");
    }

    public void play() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
            Log.e("player", "play");
        }
    }

    public void pause() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            Log.e("player", "pause");
        }
    }

    public void release() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            Log.e("player", "release");
        }
    }

    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            Log.e("player", "stop");
        }
    }

    public void stopNotification() {
//        BackgroundSoundService.this.stopForeground(false);
        stopForeground(false);
//        stopSelf();
    }

    public void stopNotification_Background() {
//        BackgroundSoundService.this.stopForeground(false);
        stopForeground(true);
        stopSelf();
    }

    private void seekTo(int time) {
        try {
            mediaPlayer.seekTo(time);
            Intent updateIntent = new Intent();
            updateIntent.putExtra("GUI_UPDATE_ACTION", "GUI_UPDATE_ACTION");
            updateIntent.putExtra("ACTUAL_TIME_VALUE_EXTRA", mediaPlayer.getCurrentPosition() / 1000);
            updateIntent.putExtra("TOTAL_TIME_VALUE_EXTRA", mediaPlayer.getDuration() / 1000);
            sendBroadcast(updateIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendInfoBroadcast() {
        if (mediaPlayer == null)
            return;

        Log.e("test", "++++++++working" + mediaPlayer.getCurrentPosition());

        Intent updateIntent = new Intent("GUI_UPDATE_ACTION");
        updateIntent.putExtra("ACTUAL_TIME_VALUE_EXTRA", mediaPlayer.getCurrentPosition() / 1000);
        updateIntent.putExtra("TOTAL_TIME_VALUE_EXTRA", mediaPlayer.getDuration() / 1000);
//        updateIntent.putExtra("msg", "msg");
        LocalBroadcastManager.getInstance(this).sendBroadcast(updateIntent);
    }

    public void createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );
            serviceChannel.enableVibration(false);
            serviceChannel.enableLights(false);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        this.stopForeground(true);
        this.stopSelf();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
