package com.app.selfpause.javaActivities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.ModelClasses.NotificationResponse;
import com.app.selfpause.R;
import com.app.selfpause.adapter.NotificationAdapter;
import com.app.selfpause.adapter.Notification_Adapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity {

    NotificationResponse resource;
    RecyclerView recyclerView;
    ImageView img_notification_back;
    String userID;
    String mypreference = "mypref", user_id = "user_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nitification_fragment);

        img_notification_back = findViewById(R.id.img_notification_back);
        recyclerView = findViewById(R.id.recyclerview_notification);

        SharedPreferences preferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = preferences.getString(user_id, "");

        LinearLayoutManager llManager_notification = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(llManager_notification);

        getNotification(userID);

        img_notification_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    public void getNotification(String userID) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<NotificationResponse> call = apiInterface.getNotification(userID);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                if (response.isSuccessful()) {
                    resource = response.body();
                    assert resource != null;
                    if (resource.getSuccess()) {
                        Notification_Adapter notificationAdapter = new Notification_Adapter(NotificationActivity.this, resource.getData());
                        recyclerView.setAdapter(notificationAdapter);
                        Log.e("size", String.valueOf(resource.getData().size()));

                    } else {
                        Toast.makeText(NotificationActivity.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(NotificationActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Toast.makeText(NotificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this, HomeActivitynew.class));
        finishAffinity();

    }
}
