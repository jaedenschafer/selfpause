package com.app.selfpause.javaActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldEditText;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.GetSocialLoginResponse;
import com.app.selfpause.ModelClasses.LoginModelClass;
import com.app.selfpause.ModelClasses.LoginSendData;
import com.app.selfpause.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivityNew extends BaseActivity {

    private static final String TAG = null;
    com.app.selfpause.Custom_Widgets.CustomBoldEditText ed_password, ed_email;
    com.app.selfpause.Custom_Widgets.CustomRegularTextView btn_login;
    CustomBoldtextView btn_signup, txt_forgot_password;
    ApiInterface apiInterface;
    private LoginSendData loginSendData = new LoginSendData();

    private int RC_SIGN_IN = 100;
    GoogleSignInOptions gso;

    String email_txt, password_txt, device_type = "Android";
    ProgressDialog progressDialog;

    private ConstraintLayout loginActivity_ll_google;

    GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    LoginButton loginButton;
    CallbackManager callbackManager;
    LoginManager loginManager;
    ConstraintLayout ll_login_facebook;
    private static final String EMAIL = "email", GOOGLE = "google", FACEBOOK = "facebook";
    public String deviceToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
        loginManager = LoginManager.getInstance();
        mAuth = FirebaseAuth.getInstance();

        ed_email = findViewById(R.id.login__email);
        ed_password = findViewById(R.id.login__password);
        btn_login = findViewById(R.id.login__txt_log_in);
        btn_signup = findViewById(R.id.txt_sign_up);
        txt_forgot_password = findViewById(R.id.txt_forgot_password);
        loginActivity_ll_google = findViewById(R.id.loginActivity_ll_google);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        initUI();


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setCanceledOnTouchOutside(false);

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivityNew.this, SignupActivityNew.class));
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email_txt = ed_email.getText().toString();
                password_txt = ed_password.getText().toString();
                loginSendData.setEmail(email_txt);
                loginSendData.setPassword(password_txt);
                loginSendData.setDeviceToken(deviceToken);
                loginSendData.setDeviceType(device_type);

                if (validateName(email_txt, ed_email, "email is required")) {
                    return;
                }
                if (validateName(password_txt, ed_password, "password is required")) {
                    return;
                }
                if (validatePassword(password_txt, ed_password, "pssword must be atleast 6 characters")) {
                    return;
                }
                showDialog();
//                    Log.e("email+", loginSendData.getEmail());
                retrofitData();
            }
        });

        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(LoginActivityNew.this, ForgetPasswordActivity.class));


            }
        });


        loginButton = findViewById(R.id.login_button_facebook_login);
        ll_login_facebook = findViewById(R.id.ll_login_facebook);

        ll_login_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                facebookSignIn();
            }
        });


        loginActivity_ll_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    private void initUI() {

        FirebaseMessaging.getInstance().subscribeToTopic("gernal")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        String msg = "Successfull";

                        if (!task.isSuccessful()) {
                            msg = "Faild";
                        }
                        Log.e("Message", msg);
//                        Toast.makeText(LoginActivityNew.this, "" + msg, Toast.LENGTH_SHORT).show();
                    }
                });

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        if (task.isSuccessful()) {

                            String token = task.getResult().getToken();
                            Log.e("GERNATE TOKEN :  ", token);
                        } else {
                            Log.e("TOKEN IS NOT GERNATED: ", task.getException().getMessage());

                        }
                    }
                });


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        deviceToken = task.getResult().getToken();

                        if (deviceToken.isEmpty()) {
                            deviceToken = UUID.randomUUID().toString();
                        }

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, deviceToken);
                        Log.d(TAG, msg);
                    }
                });
    }


    public void retrofitData() {
        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);

        Call<LoginModelClass> call = apiInterface.login(loginSendData);
        call.enqueue(new Callback<LoginModelClass>() {
            @Override
            public void onResponse(@NotNull Call<LoginModelClass> call, @NotNull Response<LoginModelClass> response) {
                if (response.isSuccessful()) {
                    LoginModelClass resource = response.body();
                    assert resource != null;
                    if (resource.getSuccess()) {
                        String code = resource.getCode();
                        String msg = resource.getMessages();

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("user_id", resource.getData().getUserId());
                        editor.putString("social_type", EMAIL);
                        editor.apply();

                        startActivity(new Intent(LoginActivityNew.this, HomeActivitynew.class));
                        Toast.makeText(LoginActivityNew.this, msg, Toast.LENGTH_SHORT).show();
                        hideDialog();
                        finishAffinity();

//                        Log.e("Success Response++++", code + " " + msg);
                    } else {
                        Toast.makeText(LoginActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                        hideDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginModelClass> call, Throwable t) {
//                Log.e("Failure Response++++", t.getMessage());
                Toast.makeText(LoginActivityNew.this, t.toString(), Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        });

    }

    public void socialLoginRetrofit(final String socialId, final String socialType, String email, String profile, String name, String deviceType, final String deviceToken) {
        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<GetSocialLoginResponse> call = apiInterface.getSocialLogin(socialId, socialType, email, profile, name, deviceType, deviceToken);

        call.enqueue(new Callback<GetSocialLoginResponse>() {
            @Override
            public void onResponse(Call<GetSocialLoginResponse> call, Response<GetSocialLoginResponse> response) {

                if (response.isSuccessful()) {

                    GetSocialLoginResponse resource = response.body();
                    if (resource.getSuccess()) {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("user_id", resource.getData().getUserId());
                        Log.e("data", socialId + "---" + deviceToken);
                        editor.putString("social_type", socialType);
                        editor.apply();

                        if (resource.getData().getUserType().equals("0")) {
                            startActivity(new Intent(LoginActivityNew.this, VoiceSelect_Activity.class));
                        } else if (resource.getData().getUserType().equals("1")) {
                            startActivity(new Intent(LoginActivityNew.this, HomeActivitynew.class));
                        }
                        Toast.makeText(LoginActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                        hideDialog();
                        finishAffinity();
                    } else {
                        Toast.makeText(LoginActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                        hideDialog();
                    }
                } else {
                    Toast.makeText(LoginActivityNew.this, response.message(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }

            @Override
            public void onFailure(Call<GetSocialLoginResponse> call, Throwable t) {
                Toast.makeText(LoginActivityNew.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        });
    }

    private boolean validateName(String name, CustomBoldEditText nameET, String err_msg) {
        if (name.isEmpty()) {
            nameET.setError(err_msg);
            nameET.requestFocus();
            return true;
        }
        return false;
    }

    public void showDialog() {
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
    }

    public void hideDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void facebookSignIn() {
        loginManager.logInWithReadPermissions(this, Arrays.asList("email"));
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

                error.printStackTrace();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }


    private void updateUI(GoogleSignInAccount user) {

        if (user != null) {

            Log.e("Update Name : ", "User : " + user.getDisplayName() + "     :::  " + user.getEmail() + "     :::  " + user.getPhotoUrl()
                    + "     :::  " + user.getId());

            String name = user.getDisplayName();
            String email = user.getEmail();
            String photoUrl = user.getPhotoUrl() + "";
            String UID = user.getId();
            try {
                String originalPieceOfUrl = "s96-c";
                String newPieceOfUrlToAdd = "s400-c";


                photoUrl = photoUrl.replace(originalPieceOfUrl, newPieceOfUrlToAdd);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //photoUrl = user.getPhotoUrl().toString();

            showDialog();
            socialLoginRetrofit(UID, GOOGLE, email, photoUrl,
                    name, device_type, deviceToken);

            signOut();
        }
    }

    private void signOut() {
        // FirebaseAuth.getInstance().signOut();
        mGoogleSignInClient.signOut();
    }


    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        GraphRequest request = GraphRequest.newMeRequest(token, (object, response) -> {
            Log.e("object", object.toString());
            try {
                String name = object.getString("name");
                String email = object.getString("email");
                String UID = object.getString("id");
                String photoUrl = "https://graph.facebook.com/" + UID + "/picture?type=large";

                showDialog();
                socialLoginRetrofit(UID, FACEBOOK, email, photoUrl,
                        name, device_type, deviceToken);


                loginManager.logOut();

            } catch (Exception e) {
                if (e.getMessage().equals("No value for email")) {
                    showToast("No Email found for this ID, Please try other emailID");
                }
                e.printStackTrace();
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }


}