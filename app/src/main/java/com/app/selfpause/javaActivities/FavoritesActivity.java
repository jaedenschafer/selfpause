package com.app.selfpause.javaActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.FavoriteModelClass.FavoritesModelClass;
import com.app.selfpause.ModelClasses.FavoriteModelClass.GetFavoritesModelClass;
import com.app.selfpause.ModelClasses.FavoriteModelClass.SubFavoritesModelClass;
import com.app.selfpause.R;
import com.app.selfpause.adapter.FavoriteAdapter.FavoritesCategoryAdapter;
import com.app.selfpause.utilityClasses.BackgroundSoundService;
import com.app.selfpause.utilityClasses.NatureSoundService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritesActivity extends AppCompatActivity {

    private RecyclerView favListRV;
    String cat_ID;
    //    String userID = "287";
    String userID;
    String mypreference = "mypref", user_id = "user_id";
    CustomBoldtextView favourite_playall;
    ApiInterface apiInterface;
    List<String> playlist;
    ImageView img_tool_bar_three_back;

    GetFavoritesModelClass resorce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_favorites_fragment);

        favListRV = findViewById(R.id.favList_RV);
        img_tool_bar_three_back = findViewById(R.id.img_tool_bar_three_back);
        favourite_playall = findViewById(R.id.favourite_playall);

        SharedPreferences preferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = preferences.getString(user_id, "");
        Log.e("aaaa", userID);

        img_tool_bar_three_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        favourite_playall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FavoritesActivity.this, CreativityAffirmationActivityNew.class);
//                intent.putExtra("demo","https://clientstagingdev.com/meditation/public/voice/1586425636.mp3");
                intent.putExtra("myFavourite", true);
                intent.putExtra("category", "Favourites");
                intent.putStringArrayListExtra("playlist", (ArrayList<String>) resorce.getData().getPlayall());
                startActivity(intent);
            }
        });

//        cat_ID = getIntent().getStringExtra("cat_id");

        getCategoryData(userID);
    }

    public void getCategoryData(String userID) {

        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);

        Call<GetFavoritesModelClass> call = apiInterface.getFavorites(userID);

        call.enqueue(new Callback<GetFavoritesModelClass>() {
            @Override
            public void onResponse(Call<GetFavoritesModelClass> call, Response<GetFavoritesModelClass> response) {

                if (response.isSuccessful()) {

                    resorce = response.body();
                    assert resorce != null;
                    if (resorce.getSuccess()) {

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FavoritesActivity.this);
                        favListRV.setLayoutManager(linearLayoutManager);
                        FavoritesCategoryAdapter favoritesCategoryAdapter = new FavoritesCategoryAdapter(FavoritesActivity.this, resorce.getData().getCategories());
                        favListRV.setAdapter(favoritesCategoryAdapter);

                        Toast.makeText(FavoritesActivity.this, "" + resorce.getMessages(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(FavoritesActivity.this, "" + resorce.getMessages(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(FavoritesActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GetFavoritesModelClass> call, Throwable t) {
                Toast.makeText(FavoritesActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCategoryData(userID);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent n_intent = new Intent(FavoritesActivity.this, BackgroundSoundService.class);
//            n_intent.putExtra("player", "Stop_notification");
//            n_intent.putExtra("player", "destroyed");
//            ContextCompat.startForegroundService(FavoritesActivity.this, n_intent);
//        }
        if (isMyServiceRunning(NatureSoundService.class)) {
            stopService(new Intent(FavoritesActivity.this, NatureSoundService.class));
        }
        if (isMyServiceRunning(BackgroundSoundService.class)) {
            stopService(new Intent(FavoritesActivity.this, BackgroundSoundService.class));
        }
//        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent m_intent = new Intent(FavoritesActivity.this, BackgroundSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(FavoritesActivity.this, m_intent);
//        }
//        if (isMyServiceRunning(NatureSoundService.class)) {
//            Intent m_intent = new Intent(FavoritesActivity.this, NatureSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(FavoritesActivity.this, m_intent);
//        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
