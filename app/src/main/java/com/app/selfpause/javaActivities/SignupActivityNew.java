package com.app.selfpause.javaActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldEditText;
import com.app.selfpause.ModelClasses.GetSocialLoginResponse;
import com.app.selfpause.ModelClasses.SignupModelClass;
import com.app.selfpause.ModelClasses.SignupSendData;
import com.app.selfpause.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Arrays;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivityNew extends BaseActivity {

    LinearLayout ll_facebook;
    TextView txt_login, txt_sign_up;
    String email_txt, password_txt, name_txt, social_id, social_type, device_type = "Android", device_token, response;
    ApiInterface apiInterface;
    private SignupSendData sendData;
    CustomBoldEditText ed_email, ed_name, ed_password;
    public String deviceToken = "";;

    private static final String TAG = null;
    private LinearLayout signupActivity_ll_google;

    private int RC_SIGN_IN = 100;
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    LoginManager loginManager;
    LoginButton loginButton;
    CallbackManager callbackManager;
    private static final String EMAIL = "email", GOOGLE = "google", FACEBOOK = "facebook";
    LinearLayout ll_login_facebook;

    ProgressDialog progressDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_two_activity);
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
        loginManager = LoginManager.getInstance();
        mAuth = FirebaseAuth.getInstance();
        // setStatusBaseWhiteMain();
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setCanceledOnTouchOutside(false);


        ed_email = findViewById(R.id.signup__email);
        ed_name = findViewById(R.id.signup__name);
        ed_password = findViewById(R.id.signup__password);


        txt_login = (TextView) findViewById(R.id.signup_txt_login);
        txt_sign_up = (TextView) findViewById(R.id.signup__txt_sign_up);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent voice = new Intent(SignupActivityNew.this, LoginActivityNew.class);
                startActivity(voice);
            }
        });


        txt_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name_txt = ed_name.getText().toString();
                email_txt = ed_email.getText().toString();
                password_txt = ed_password.getText().toString();

                Log.e("email", email_txt);


                if (validateName(name_txt, ed_name, "name is required")) {
                    return;
                }
                if (validateName(email_txt, ed_email, "email is required")) {
                    return;
                }
                if (validateName(password_txt, ed_password, "password is required")) {
                    return;
                }
                if (validatePassword(password_txt, ed_password, "pssword must be atleast 6 characters")) {
                    return;
                }

                sendData = new SignupSendData();
                sendData.setFirstName(name_txt);
                sendData.setEmail(email_txt);
                sendData.setPassword(password_txt);
                sendData.setDeviceType(device_type);
                sendData.setDeviceToken(UUID.randomUUID().toString());
//                Log.e("email+", sendData.getEmail());
                retrofitData();

                showDialog();
            }
        });

//        signInButton=findViewById(R.id.signinwithGogle);
        signupActivity_ll_google = findViewById(R.id.signup_ll_google);

        signupActivity_ll_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();

            }
        });
        ll_login_facebook = findViewById(R.id.ll_facebook_signup);
        ll_login_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                facebookSignIn();
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        if (task.isSuccessful()) {

                            String token = task.getResult().getToken();
                            Log.e("GERNATE TOKEN :  ", token);
                        } else {
                            Log.e("TOKEN IS NOT GERNATED: ", task.getException().getMessage());

                        }
                    }
                });


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        deviceToken = task.getResult().getToken();

                        if (deviceToken.isEmpty()) {
                            deviceToken = UUID.randomUUID().toString();
                        }

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, deviceToken);
                        Log.d(TAG, msg);
                    }
                });

    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    public void retrofitData() {
        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);

        Call<SignupModelClass> call = apiInterface.signup(sendData);
        call.enqueue(new Callback<SignupModelClass>() {
            @Override
            public void onResponse(Call<SignupModelClass> call, Response<SignupModelClass> response) {

                if (response.isSuccessful()) {
                    SignupModelClass resource = response.body();
                    Toast.makeText(SignupActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                    assert resource != null;
                    if (resource.getSuccess()) {
                        String code = resource.getCode();
                        String msg = resource.getMessages();

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("user_id", resource.getData().getUserId());
                        editor.putString("social_type", EMAIL);
                        editor.putBoolean("voice_selected", false);
                        editor.putBoolean("category_selected", false);
                        editor.apply();


                        startActivity(new Intent(SignupActivityNew.this, VoiceSelect_Activity.class));
                        finish();
                        Toast.makeText(SignupActivityNew.this, msg, Toast.LENGTH_SHORT).show();

                        hideDialog();

                        Log.e("Success Response++++", code + " " + msg + " " + resource.getData().getUserId());
                    } else {
                        Toast.makeText(SignupActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();

                        hideDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<SignupModelClass> call, Throwable t) {
                Log.e("Failure Response++++", t.getMessage());
                Toast.makeText(SignupActivityNew.this, t.toString(), Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        });
    }

    public void socialLoginRetrofit(final String socialId, final String socialType, String email, String profile, String name, String deviceType, final String deviceToken) {
        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<GetSocialLoginResponse> call = apiInterface.getSocialLogin(socialId, socialType, email, profile, name, deviceType, deviceToken);

        call.enqueue(new Callback<GetSocialLoginResponse>() {
            @Override
            public void onResponse(Call<GetSocialLoginResponse> call, Response<GetSocialLoginResponse> response) {

                if (response.isSuccessful()) {

                    GetSocialLoginResponse resource = response.body();
                    if (resource.getSuccess()) {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("user_id", resource.getData().getUserId());
                        Log.e("data", socialId + "---" + deviceToken);
                        editor.putString("social_type", socialType);
                        editor.apply();

                        if (resource.getData().getUserType().equals("0")) {
                            startActivity(new Intent(SignupActivityNew.this, VoiceSelect_Activity.class));
                        } else if (resource.getData().getUserType().equals("1")) {
                            startActivity(new Intent(SignupActivityNew.this, HomeActivitynew.class));
                        }
                        Toast.makeText(SignupActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                        hideDialog();
                        finishAffinity();
                    } else {
                        Toast.makeText(SignupActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                        hideDialog();
                    }
                } else {
                    Toast.makeText(SignupActivityNew.this, response.message(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }

            @Override
            public void onFailure(Call<GetSocialLoginResponse> call, Throwable t) {
                Toast.makeText(SignupActivityNew.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        });
    }


            private boolean validateName(String name, CustomBoldEditText nameET, String err_msg) {
        if (name.isEmpty()) {
            nameET.setError(err_msg);
            nameET.requestFocus();
            return true;
        }
        return false;
    }

    public void showDialog() {

        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
    }

    public void hideDialog() {

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void facebookSignIn() {
        loginManager.logInWithReadPermissions(this, Arrays.asList("email"));
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

                error.printStackTrace();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount user) {

//        if (user != null) {
//
//            Log.e("Update Name : ", "User : " + user.getDisplayName() + "     :::  " + user.getEmail() + "     :::  " + user.getPhotoUrl()
//                    + "     :::  " + user.getId());
//
//            String name = user.getDisplayName();
//            String email = user.getEmail();
//            String photoUrl = user.getPhotoUrl() + "";
//            String UID = user.getId();
//            String originalPieceOfUrl = "s96-c";
//            String newPieceOfUrlToAdd = "s400-c";
//
//
//            photoUrl = photoUrl.replace(originalPieceOfUrl, newPieceOfUrlToAdd);
//
//            //photoUrl = user.getPhotoUrl().toString();
//
//
//            sendData = new SignupSendData();
//            sendData.setFirstName(name);
//            sendData.setEmail(email);
//            sendData.setDeviceType(device_type);
//            sendData.setDeviceToken(UUID.randomUUID().toString());
//            sendData.setSocialType(GOOGLE);
//            sendData.setSocialId(UID);
//
//            Log.e("socialid", UID);
//            retrofitData();
//
//
//            signOut();
//        }
        if (user != null) {

            Log.e("Update Name : ", "User : " + user.getDisplayName() + "     :::  " + user.getEmail() + "     :::  " + user.getPhotoUrl()
                    + "     :::  " + user.getId());

            String name = user.getDisplayName();
            String email = user.getEmail();
            String photoUrl = user.getPhotoUrl() + "";
            String UID = user.getId();
            try {
                String originalPieceOfUrl = "s96-c";
                String newPieceOfUrlToAdd = "s400-c";


                photoUrl = photoUrl.replace(originalPieceOfUrl, newPieceOfUrlToAdd);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //photoUrl = user.getPhotoUrl().toString();

            showDialog();
            socialLoginRetrofit(UID, GOOGLE, email, photoUrl,
                    name, device_type, deviceToken);

            signOut();
        }
    }

    private void signOut() {
        // FirebaseAuth.getInstance().signOut();
        mGoogleSignInClient.signOut();
    }

    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        GraphRequest request = GraphRequest.newMeRequest(token, (object, response) -> {
            Log.e("object", object.toString());
            try {
                String name = object.getString("name");
                String email = object.getString("email");
                String UID = object.getString("id");
                String photoUrl = "https://graph.facebook.com/" + UID + "/picture?type=large";

//                sendData = new SignupSendData();
//                sendData.setFirstName(name);
//                sendData.setEmail(email);
//                sendData.setDeviceType(device_type);
//                sendData.setDeviceToken(UUID.randomUUID().toString());
//                sendData.setSocialType(FACEBOOK);
//                sendData.setSocialId(UID);

//                Log.e("socialid", UID);
//                retrofitData();
                showDialog();
                socialLoginRetrofit(UID, FACEBOOK, email, photoUrl,
                        name, device_type, deviceToken);

                loginManager.logOut();

            } catch (Exception e) {
                if (e.getMessage().equals("No value for email")) {
                    showToast("No Email found for this ID, Please try other emailID");
                }
                e.printStackTrace();
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

}
