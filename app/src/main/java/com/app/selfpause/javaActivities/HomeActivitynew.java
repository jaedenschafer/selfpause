package com.app.selfpause.javaActivities;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.JavaFragment.AccountFragment;
import com.app.selfpause.JavaFragment.LibraryFragmentNew;
import com.app.selfpause.JavaFragment.RecordFragmentNew;
import com.app.selfpause.JavaFragment.SoundFragment;
import com.app.selfpause.ModelClasses.GetHomeResponse;
import com.app.selfpause.ModelClasses.GetProfileResponse;
import com.app.selfpause.R;
import com.app.selfpause.utilityClasses.BackgroundSoundService;
import com.app.selfpause.utilityClasses.NatureSoundService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivitynew extends BaseActivity {

    private LinearLayout lib, sound, record, account;
    public static AppCompatImageView img_bottom_lib, img_bottom_sound, img_bottom_record, img_bottom_account;
    private FrameLayout container;
    private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
    boolean check;
    String userID;
    String mypreference = "mypref", user_id = "user_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("MyNotifications", "MyNotifications", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            assert manager != null;
            manager.createNotificationChannel(channel);
        }

//        FirebaseApp.initializeApp(this);


//        TOKEN :- cY5EUC_GSJ6pcYM2fRyHNk:APA91bH--FIucex3FIUgjaGYGvTIXJO-KRb0s9x2ismOLvyOXS6IYSLJBLD5xHnLI83mw_OCtkFbg3qEkXl9fLLlW4cBLPqWmNeWRNs9koMe8h61gk0bCVIeyHbjwxHIHroboz9qusPU

        lib = findViewById(R.id.lib);
        sound = findViewById(R.id.sound);
        record = findViewById(R.id.record);
        account = findViewById(R.id.account);
        img_bottom_lib = findViewById(R.id.img_bottom_lib);
        img_bottom_sound = findViewById(R.id.img_bottom_sound);
        img_bottom_record = findViewById(R.id.img_bottom_record);
        img_bottom_account = findViewById(R.id.img_bottom_account);
        container = findViewById(R.id.container);

        SharedPreferences sharedPreferences = getSharedPreferences("myPref", 0);
//        sharedPreferences.getString("pref","");
        check = sharedPreferences.getBoolean("Payment", true);
        SharedPreferences preferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = preferences.getString(user_id, "");

        profileData(userID);

//        if (check){
//
////            check = false;
//            SharedPreferences sharedPreferences1 = getSharedPreferences("myPref",0);
//            SharedPreferences.Editor editor = sharedPreferences1.edit();
//            editor.putBoolean("Payment",true);
//            editor.apply();
//
//            img_bottom_lib.setVisibility(View.GONE);
//            img_bottom_sound.setVisibility(View.VISIBLE);
//            img_bottom_record.setVisibility(View.GONE);
//            img_bottom_account.setVisibility(View.GONE);
//
//            SoundFragment soundFragment = new SoundFragment();
//            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.replace(R.id.container,soundFragment);
//            fragmentTransaction.addToBackStack("");
//            fragmentTransaction.commit();
//
//        }
//        else {
//
////            check = true;
////            sharedPreferences.getBoolean("Payment",false);
//
//            img_bottom_lib.setVisibility(View.VISIBLE);
//            img_bottom_sound.setVisibility(View.GONE);
//            img_bottom_record.setVisibility(View.GONE);
//            img_bottom_account.setVisibility(View.GONE);
//
//            LibraryFragmentNew libraryFragmentNew = new LibraryFragmentNew();
//            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.replace(R.id.container,libraryFragmentNew);
//            fragmentTransaction.addToBackStack("");
//            fragmentTransaction.commit();
//
//
//
////            paymentData();
//        }

        img_bottom_lib.setVisibility(View.VISIBLE);
        img_bottom_sound.setVisibility(View.GONE);
        img_bottom_record.setVisibility(View.GONE);
        img_bottom_account.setVisibility(View.GONE);

        LibraryFragmentNew libraryFragmentNew = new LibraryFragmentNew();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, libraryFragmentNew);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();


        lib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                img_bottom_lib.setVisibility(View.VISIBLE);
                img_bottom_sound.setVisibility(View.GONE);
                img_bottom_record.setVisibility(View.GONE);
                img_bottom_account.setVisibility(View.GONE);

                LibraryFragmentNew libraryFragmentNew = new LibraryFragmentNew();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, libraryFragmentNew, TAG_FRAGMENT);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();

            }
        });

        sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                img_bottom_lib.setVisibility(View.GONE);
                img_bottom_sound.setVisibility(View.VISIBLE);
                img_bottom_record.setVisibility(View.GONE);
                img_bottom_account.setVisibility(View.GONE);

                SoundFragment soundFragment = new SoundFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, soundFragment, TAG_FRAGMENT);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();

            }
        });

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                img_bottom_lib.setVisibility(View.GONE);
                img_bottom_sound.setVisibility(View.GONE);
                img_bottom_record.setVisibility(View.VISIBLE);
                img_bottom_account.setVisibility(View.GONE);

                RecordFragmentNew recordFragment = new RecordFragmentNew();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, recordFragment, TAG_FRAGMENT);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();

            }
        });

        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                img_bottom_lib.setVisibility(View.GONE);
                img_bottom_sound.setVisibility(View.GONE);
                img_bottom_record.setVisibility(View.GONE);
                img_bottom_account.setVisibility(View.VISIBLE);

                AccountFragment accountFragment = new AccountFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, accountFragment, TAG_FRAGMENT);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            if (doubleBackToExitPressedOnce) {
                finishAffinity();
                return;
            }
            this.doubleBackToExitPressedOnce = true;

            showToast(getString(R.string.exit_msg));
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);

        }

    }

    public void paymentData() {

        img_bottom_lib.setVisibility(View.GONE);
        img_bottom_sound.setVisibility(View.VISIBLE);
        img_bottom_record.setVisibility(View.GONE);
        img_bottom_account.setVisibility(View.GONE);

        SoundFragment soundFragment = new SoundFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, soundFragment, TAG_FRAGMENT);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }

    public void profileData(String userID) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<GetProfileResponse> call = apiInterface.getProfile(userID);
        call.enqueue(new Callback<GetProfileResponse>() {
            @Override
            public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {

                if (response.isSuccessful()) {
                    GetProfileResponse resource = response.body();
                    assert resource != null;
                    if (resource.getSuccess()) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        pref.edit().remove("is_notification").apply();
                        pref.edit().remove("profile_img").apply();
                        pref.edit().remove("name").apply();
                        editor.putString("name", resource.getData().getFirstName());
                        editor.putString("profile_img", resource.getData().getProfile());
                        editor.putString("is_notification", resource.getData().getIsNotification());
                        Log.e("asss", resource.getData().getFirstName());
                        editor.apply();
                    }

                } else {
                    Toast.makeText(HomeActivitynew.this, response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GetProfileResponse> call, Throwable t) {
                Toast.makeText(HomeActivitynew.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent n_intent = new Intent(HomeActivitynew.this, BackgroundSoundService.class);
//            n_intent.putExtra("player", "Stop_notification");
//            n_intent.putExtra("player", "destroyed");
//            ContextCompat.startForegroundService(HomeActivitynew.this, n_intent);
//        }
        if (isMyServiceRunning(NatureSoundService.class)) {
            stopService(new Intent(HomeActivitynew.this, NatureSoundService.class));
        }
        if (isMyServiceRunning(BackgroundSoundService.class)) {
            stopService(new Intent(HomeActivitynew.this, BackgroundSoundService.class));
        }
//        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent m_intent = new Intent(HomeActivitynew.this, BackgroundSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(HomeActivitynew.this, m_intent);
//        }
//        if (isMyServiceRunning(NatureSoundService.class)) {
//            Intent m_intent = new Intent(HomeActivitynew.this, NatureSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(HomeActivitynew.this, m_intent);
//        }

    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
