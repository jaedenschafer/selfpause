package com.app.selfpause.javaActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.EditVoiceData;
import com.app.selfpause.ModelClasses.EditVoiceResponse;
import com.app.selfpause.ModelClasses.GetUserVoiceData;
import com.app.selfpause.ModelClasses.GetUserVoiceResponse;
import com.app.selfpause.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivityNew extends AppCompatActivity {

    ImageView notification_back;
    CustomBoldtextView time;
    AppCompatImageView notification_toggleButton_off, notification_toggleButton_on;
    String status = "0";
    int mHour, mMinute;
    String userID, userVoiceId;
    String mypreference = "mypref", user_id = "user_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_new);

        notification_back = findViewById(R.id.notification_back);
        time = findViewById(R.id.notification_select_time);
        notification_toggleButton_off = findViewById(R.id.notification_toggleButton_off);
        notification_toggleButton_on = findViewById(R.id.notification_toggleButton_on);

        SharedPreferences preferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = preferences.getString(user_id, "");

        notification_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getUserVoice(userID);

//        final Calendar c = Calendar.getInstance();
//        mHour = c.get(Calendar.HOUR_OF_DAY);
//        mMinute = c.get(Calendar.MINUTE);
//
//        if (mMinute < 10) {
//            if (mHour < 10) {
//                time.setText("0" + String.valueOf(mHour) + ":0" + String.valueOf(mMinute));
//            } else {
//                time.setText(String.valueOf(mHour) + ":0" + String.valueOf(mMinute));
//            }
//        } else {
//            if (mHour < 10) {
//                time.setText("0" + String.valueOf(mHour) + ":0" + String.valueOf(mMinute));
//            } else {
//                time.setText(String.valueOf(mHour) + ":" + String.valueOf(mMinute));
//            }
//        }

        LinearGradient shade = new LinearGradient(0f, 0f, 0f, time.getTextSize(), getResources().getColor(R.color.time_color_a), getResources().getColor(R.color.time_color_b), Shader.TileMode.CLAMP);
        time.getPaint().setShader(shade);

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Launch Time Picker Dialog
//                TimePickerDialog timePickerDialog = new TimePickerDialog(VoiceSelect_Activity.this,
//                        new TimePickerDialog.OnTimeSetListener() {
//
//                            @Override
//                            public void onTimeSet(TimePicker view, int hourOfDay,
//                                                  int minute) {
//
//                                time.setText(hourOfDay + ":" + minute);
//                            }
//                        }, mHour, mMinute, false);
//                timePickerDialog.show();


                View dialogView = getLayoutInflater().inflate(R.layout.time_picker_dialog, null);
                final BottomSheetDialog dialog = new BottomSheetDialog(NotificationActivityNew.this);
                dialog.setContentView(dialogView);
                dialog.show();
//
                final TimePicker timePicker = (TimePicker) dialog.findViewById(R.id.time_picker);
                TextView cancel_txt = (TextView) dialog.findViewById(R.id.timePicker_txt_cancel);
                TextView done_txt = (TextView) dialog.findViewById(R.id.timePicker_txt_done);

                assert cancel_txt != null;
                cancel_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                assert timePicker != null;
                timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                        if (i >= 0 && i < 12) {
                            if (i1 < 10) {
                                time.setText("" + i + ":0" + i1 + "AM");
                            } else {
                                time.setText("" + i + ":" + i1 + " AM");
                            }
                        } else {
                            if (i == 12) {
                                if (i1 < 10) {
                                    time.setText("" + i + ":0" + i1 + "PM");
                                } else {
                                    time.setText("" + i + ":" + i1 + " PM");
                                }
                            } else {
                                i = i - 12;
                                if (i1 < 10) {
                                    time.setText("" + i + ":0" + i1 + "PM");
                                } else {
                                    time.setText("" + i + ":" + i1 + " PM");
                                }
                            }
                        }
                    }
                });
                assert done_txt != null;
                done_txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setTimeStatus(userID, userVoiceId, time.getText().toString(), status);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        notification_toggleButton_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "1";
                notification_toggleButton_off.setVisibility(View.GONE);
                notification_toggleButton_on.setVisibility(View.VISIBLE);
                setTimeStatus(userID, userVoiceId, time.getText().toString(), status);
            }
        });

        notification_toggleButton_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "0";
                notification_toggleButton_off.setVisibility(View.VISIBLE);
                notification_toggleButton_on.setVisibility(View.GONE);
                setTimeStatus(userID, userVoiceId, time.getText().toString(), status);
            }
        });
    }

    public void getUserVoice(String userID) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<GetUserVoiceResponse> call = apiInterface.getUserVoice(userID);
        call.enqueue(new Callback<GetUserVoiceResponse>() {
            @Override
            public void onResponse(Call<GetUserVoiceResponse> call, Response<GetUserVoiceResponse> response) {
                if (response.isSuccessful()) {
                    GetUserVoiceResponse resource = response.body();
                    assert resource != null;
                    if (resource.getSuccess()) {
                        time.setText(resource.getData().get(0).getUserTime().toString());
                        userVoiceId = resource.getData().get(0).getUserVoiceId().toString();
                        if (resource.getData().get(0).getOnOffStatus().equals(0)) {
                            status = "0";
                            notification_toggleButton_off.setVisibility(View.VISIBLE);
                            notification_toggleButton_on.setVisibility(View.GONE);
                        } else if (resource.getData().get(0).getOnOffStatus().equals(1)) {
                            status = "1";
                            notification_toggleButton_off.setVisibility(View.GONE);
                            notification_toggleButton_on.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(NotificationActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetUserVoiceResponse> call, Throwable t) {
                Toast.makeText(NotificationActivityNew.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setTimeStatus(String userID, String userVoiceId, String time, String status) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<EditVoiceResponse> call = apiInterface.setEditVoice(userID, userVoiceId, time, status);
        call.enqueue(new Callback<EditVoiceResponse>() {
            @Override
            public void onResponse(Call<EditVoiceResponse> call, Response<EditVoiceResponse> response) {
                if (response.isSuccessful()) {
                    EditVoiceResponse resource = response.body();
                    if (resource.getSuccess()) {
                        Toast.makeText(NotificationActivityNew.this, "updated", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(NotificationActivityNew.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<EditVoiceResponse> call, Throwable t) {
                Toast.makeText(NotificationActivityNew.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}