package com.app.selfpause.javaActivities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.LogoutModelClass;
import com.app.selfpause.ModelClasses.NotificationStateData;
import com.app.selfpause.ModelClasses.NotificationStateResponse;
import com.app.selfpause.R;
import com.app.selfpause.adapter.Notification_Adapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    private CustomBoldtextView txt_logout, txt_privacy, txt_terms_condition, txt_help_center, txt_support, txt_account, txt_notification, txt_subscription;
    private ImageView img_account_back;
    AppCompatImageView notification_off, notification_on;
//    LinearLayout settings_processing;

    private ApiInterface apiInterface;
    LogoutModelClass logoutModelClass = new LogoutModelClass();

    String userID,isNotification;
    String mypreference = "mypref", user_id = "user_id", is_Notification = "is_notification";

    Dialog dialog;

    public static final int SIGN_IN = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_fragment);

        txt_logout = findViewById(R.id.txt_logout);
        txt_privacy = findViewById(R.id.txt_privacy);
        txt_terms_condition = findViewById(R.id.txt_terms_condition);
        img_account_back = findViewById(R.id.img_account_first_back);
        txt_help_center = findViewById(R.id.txt_help_center);
        txt_support = findViewById(R.id.txt_support);
        txt_notification = findViewById(R.id.txt_notification);
        txt_subscription = findViewById(R.id.txt_sub);
        notification_off = findViewById(R.id.notification_off);
        notification_on = findViewById(R.id.notification_on);
//        settings_processing = findViewById(R.id.settings_processing);

        SharedPreferences preferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = preferences.getString(user_id, "");
        isNotification = preferences.getString(is_Notification,"0");

       /* assert isNotification != null;
        if (isNotification.equals("0")){
            notification_off.setVisibility(View.VISIBLE);
            notification_on.setVisibility(View.GONE);
        }else {
            notification_off.setVisibility(View.GONE);
            notification_on.setVisibility(View.VISIBLE);
        }*/

        img_account_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        txt_account = findViewById(R.id.txt_account);

        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, LogoutActivity.class);
                startActivity(intent);
            }
        });

        txt_account.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(SettingActivity.this, AccountSettingActivityNew.class);
                startActivity(intent);
            }
        });

        txt_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, NotificationActivityNew.class);
                startActivity(intent);
            }
        });

        notification_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                settings_processing.setVisibility(View.VISIBLE);
                dialog.show();
                notificationState(userID, "1");
            }
        });

        notification_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                settings_processing.setVisibility(View.VISIBLE);
                dialog.show();
                notificationState(userID, "0");
            }
        });

        txt_terms_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, TermsAndConditionActivity.class);
                startActivity(intent);

            }
        });

        txt_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, PrivacyPolicyActivity.class);
                startActivity(intent);
            }
        });

        txt_help_center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SettingActivity.this, HelpCenterNew.class));

            }
        });

        txt_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SettingActivity.this, SupportActivity_new.class));

            }
        });

        txt_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, SubscriptionActivity.class);
                startActivity(intent);
            }
        });

        dialog = new Dialog(SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    private void notificationState(String userID, String s) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<NotificationStateResponse> call = apiInterface.getNotificationState(userID, s);
        call.enqueue(new Callback<NotificationStateResponse>() {
            @Override
            public void onResponse(Call<NotificationStateResponse> call, Response<NotificationStateResponse> response) {
                if (response.isSuccessful()) {
                    NotificationStateResponse resource = response.body();
                    assert resource != null;
                    if (resource.getSuccess()) {
                        if (resource.getData().getOn().equals("0")) {
                            notification_off.setVisibility(View.VISIBLE);
                            notification_on.setVisibility(View.GONE);
                            dialog.dismiss();
//                            settings_processing.setVisibility(View.GONE);
                        } else if (resource.getData().getOn().equals("1")) {
                            notification_off.setVisibility(View.GONE);
                            notification_on.setVisibility(View.VISIBLE);
                            dialog.dismiss();
//                            settings_processing.setVisibility(View.GONE);
                        }

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        pref.edit().remove("is_notification").apply();
                        editor.putString("is_notification", resource.getData().getOn());
                        editor.apply();

                    } else {
                        Toast.makeText(SettingActivity.this, resource.getMessages(), Toast.LENGTH_SHORT).show();
//                        settings_processing.setVisibility(View.GONE);
                        dialog.dismiss();
                    }
                } else {
                    Toast.makeText(SettingActivity.this, response.message(), Toast.LENGTH_SHORT).show();
//                    settings_processing.setVisibility(View.GONE);
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<NotificationStateResponse> call, Throwable t) {
                Toast.makeText(SettingActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//                settings_processing.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });
    }

    public void goToLoginActivity() {
        startActivity(new Intent(SettingActivity.this, LoginActivityNew.class));
        finishAffinity();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        refreshActivity();
        super.onBackPressed();
    }

    private void refreshActivity() {
        Intent i = new Intent(SettingActivity.this, HomeActivitynew.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}
