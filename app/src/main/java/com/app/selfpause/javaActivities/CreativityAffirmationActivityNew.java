package com.app.selfpause.javaActivities;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.JavaFragment.LibraryFragmentNew;
import com.app.selfpause.ModelClasses.CheckVoiceResponse;
import com.app.selfpause.ModelClasses.GetFavouriteSession;
import com.app.selfpause.ModelClasses.MusicPlayerGetVoice;
import com.app.selfpause.ModelClasses.MusicPlayerResponse;
import com.app.selfpause.R;
import com.app.selfpause.adapter.PlayerNatureAdapter;
import com.app.selfpause.utilityClasses.BackgroundSoundService;
import com.app.selfpause.utilityClasses.NatureSoundService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.tankery.lib.circularseekbar.CircularSeekBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreativityAffirmationActivityNew extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public static LinearLayout img_vol_bar, nature_loading;
    public static RelativeLayout ll_options;
    ImageView musicbtn, back_btn, img_heart;
    static AppCompatImageView player_play, player_backward, player_forward, player_vol_low, player_vol_high;
    CustomBoldtextView player_timer, name, category_title, song_title;
    String song, userID, profileImg, nametxt, category_txt, song_txt, type, sessionId;
    int total_duration, current_time, volume, default_volume;
    public Boolean playing, defaultVoice;
    public static Boolean back = true;
    Spinner spinner_voice;
    CircularSeekBar circularSeekBar;
    SeekBar player_vol_bar;
    private boolean blockGUIUpdate;
    AudioManager audioManager;
    public static RecyclerView nature_recycler;
    ArrayList<String> playallList;
    ApiInterface apiInterface;
    String mypreference = "mypref", user_id = "user_id", profile_Img = "profile_img", name_txt = "name", voice_id, fav_session;
    CircleImageView voice_img;
    List<MusicPlayerGetVoice> voices;
    ArrayList<String> voiceList = new ArrayList<>();
    Boolean myRecording, myFavourite;
    public static String CHANNEL_ID = "ForegroundService";
    CheckVoiceResponse checkVoiceResponse;
    public static Dialog dialog;
    public static Handler seekbarUpdateHandler;
    public static Runnable updateSeekBar;
    //    private GuiReceiver receiver;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creativity_two_fragment);

//        SharedPreferences preferences = getSharedPreferences("mypref", Context.MODE_PRIVATE);
//        playing = preferences.getBoolean("playing", false);
        SharedPreferences pref = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = pref.getString(user_id, "");
        profileImg = pref.getString(profile_Img, "");
        nametxt = pref.getString(name_txt, "");

        ll_options = findViewById(R.id.ll_options);
        musicbtn = findViewById(R.id.img_music);
        player_vol_bar = findViewById(R.id.player_vol_bar);
        player_play = findViewById(R.id.player_play);
        player_timer = findViewById(R.id.player_timer);
        back_btn = findViewById(R.id.img_back_four);
        circularSeekBar = findViewById(R.id.seekbar);
        spinner_voice = findViewById(R.id.spinner_voice);
        img_vol_bar = findViewById(R.id.player_vol);
        nature_recycler = findViewById(R.id.player_nature_recyclerView);
        player_backward = findViewById(R.id.player_backward);
        player_forward = findViewById(R.id.player_forward);
        player_vol_low = findViewById(R.id.player_vol_low);
        player_vol_high = findViewById(R.id.player_vol_high);
        voice_img = findViewById(R.id.voice_img);
        name = findViewById(R.id.name);
        category_title = findViewById(R.id.category_title);
        song_title = findViewById(R.id.song_title);
        img_heart = findViewById(R.id.img_heart);
        nature_loading = findViewById(R.id.nature_loading);
        back = true;

        spinner_voice.setOnItemSelectedListener(this);

        playallList = getIntent().getStringArrayListExtra("playlist");
//        Log.e("playlist",playallList.toString());

        myRecording = getIntent().getBooleanExtra("myRecording", false);
        myFavourite = getIntent().getBooleanExtra("myFavourite", false);
        category_txt = getIntent().getStringExtra("category");
        song_txt = getIntent().getStringExtra("song_name");
        type = getIntent().getStringExtra("type");
        sessionId = getIntent().getStringExtra("session_id");
        fav_session = getIntent().getStringExtra("fav_session");
        Log.e("dasd", userID);

        category_title.setText(category_txt);
        song_title.setText(song_txt);

        LibraryFragmentNew.update = false;

//        dialog = new Dialog(CreativityAffirmationActivityNew.this, R.style.CustomDialogTheme);
//        dialog.setContentView(R.layout.dialog_loading);
//        dialog.setCancelable(false);
//        Window window = dialog.getWindow();
//        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        dialog.show();

        if (type == null) {
            type = "recording & favourite";
        }

        if (!type.equals("affirmation")) {
            song = getIntent().getStringExtra("song");
            if (song == null) {
                if (playallList.size() != 0) {
                    song = playallList.get(0);
                }
                spinner_voice.setVisibility(View.INVISIBLE);
                voice_img.setVisibility(View.INVISIBLE);
//                category_title.setVisibility(View.INVISIBLE);
                song_title.setVisibility(View.INVISIBLE);
                name.setVisibility(View.GONE);
                playSong();
//            song = "https://clientstagingdev.com/meditation/public/voice/1586425636.mp3";
            }
        }

        if (type != null) {
            if (type.equals("nature")) {
                spinner_voice.setVisibility(View.INVISIBLE);
                voice_img.setVisibility(View.INVISIBLE);
                category_title.setVisibility(View.INVISIBLE);
                playSong();
            } else if (type.equals("affirmation")) {
                if (fav_session.equals("1")) {
                    img_heart.setVisibility(View.VISIBLE);
                    img_heart.setImageResource(R.mipmap.shaed_heart_pink);
                } else {
                    img_heart.setVisibility(View.VISIBLE);
                }
//                favouriteSwitch(userID,sessionId);
            }
        }

        img_heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favouriteSwitch(userID, sessionId);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CreativityAffirmationActivityNew.this, LinearLayoutManager.HORIZONTAL, false);
        nature_recycler.setLayoutManager(linearLayoutManager);

        checkVoice(userID);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Log.e("switch", "a");

        circularSeekBar.setMax(total_duration);

        circularSeekBar.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            int time;

            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, float progress, boolean fromUser) {
                time = (int) progress;
//
                circularSeekBar.setProgress(this.time);
                if (fromUser)
                    player_timer.setText(getTimeString(time));
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
                unblockGUIUpdate();
                Intent m_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
                m_intent.putExtra("seek_to", time * 1000);
                m_intent.putExtra("player", "ACTION_SEEK_TO");
                ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, m_intent);
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
                blockGUIUpdate = true;
            }
        });

        circularSeekBar.setPointerColor(ResourcesCompat.getColor(getResources(), R.color.weight_loss_pointer, null));
        circularSeekBar.setCircleProgressColor(ResourcesCompat.getColor(getResources(), R.color.weight_loss, null));
        circularSeekBar.setPointerHaloColor(ResourcesCompat.getColor(getResources(), R.color.white, null));

        try {
            CreativityAffirmationActivityNew.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!LibraryFragmentNew.update) {
                        Intent intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
                        intent.putExtra("player", "ACTION_SEND_INFO");

                        try {
                            ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        handler.postDelayed(this, 1000);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//            seekbarUpdateHandler = new Handler();
//            updateSeekBar = new Runnable() {
//                @Override
//                public void run() {
//                    Intent intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
//                    intent.putExtra("player", "ACTION_SEND_INFO");
//
//                    try {
//                        ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, intent);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            };
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            volume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);
            default_volume = volume;
        }
        player_vol_bar.setProgress(volume);
        player_vol_bar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM));

        player_vol_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class);
                n_intent.putExtra("player", "Vol_bar");
                n_intent.putExtra("volume", progress);
                startService(n_intent);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        player_vol_low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class);
                n_intent.putExtra("player", "Vol_low");
                startService(n_intent);

            }
        });

        player_vol_high.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class);
                n_intent.putExtra("player", "Vol_high");
                startService(n_intent);

            }
        });

        musicbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_options.getVisibility() == View.VISIBLE) {
                    ll_options.setVisibility(View.INVISIBLE);
                    if (img_vol_bar.getVisibility() == View.VISIBLE) {
                        img_vol_bar.setVisibility(View.INVISIBLE);
                    }
                    if (isMyServiceRunning(NatureSoundService.class)) {
                        stopService(new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class));
                    }
                } else {
                    ll_options.setVisibility(View.VISIBLE);
                }
            }
        });

//        ll_options.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                img_vol_bar.setVisibility(View.VISIBLE);
//                if (isMyServiceRunning(NatureSoundService.class)) {
//                    stopService(new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class));
//                } else {
//                    Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class);
//                    n_intent.putExtra("nature_song", "https://clientstagingdev.com/meditation/public/voice/1586425636.mp3");
//                    n_intent.putExtra("player", "Play");
//                    startService(n_intent);
//                }
//            }
//        });

        player_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMyServiceRunning(BackgroundSoundService.class)) {
                    try {
                        if (playing) {
                            player_play.setImageResource(R.mipmap.player_sound_play);
                            Intent m_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
                            m_intent.putExtra("player", "Pause");
                            ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, m_intent);
                            if (isMyServiceRunning(NatureSoundService.class)) {
                                Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class);
                                n_intent.putExtra("player", "Pause");
                                startService(n_intent);
                            }
                            playing = false;
                            Log.e("switch", "b");
                        } else {
                            player_play.setImageResource(R.mipmap.player_pause);
                            Intent m_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
                            m_intent.putExtra("player", "Resume");
                            ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, m_intent);
                            if (isMyServiceRunning(NatureSoundService.class)) {
                                Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class);
                                n_intent.putExtra("player", "Resume");
                                startService(n_intent);
                            }
                            playing = true;
                            Log.e("switch", "c");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        playing = true;
                    }
                }
            }
        });

        player_backward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
                n_intent.putExtra("player", "Seek_backward");
                ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, n_intent);
            }
        });

        player_forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
                n_intent.putExtra("player", "Seek_forward");
                ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, n_intent);
            }
        });

    }

    private void unblockGUIUpdate() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                blockGUIUpdate = false;
            }
        }, 150);
    }

    private static String getTimeString(int totalTime) {
        long s = totalTime % 60;
        long m = (totalTime / 60) % 60;
        long h = totalTime / 3600;

        String stringTotalTime;
        if (h != 0)
            stringTotalTime = String.format("%02d:%02d:%02d", h, m, s);
        else
            stringTotalTime = String.format("%02d:%02d", m, s);
        return stringTotalTime;
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("player_state", 0);
        SharedPreferences.Editor editor = preferences.edit();

        if (preferences.getBoolean("pause", false)) {
            editor.putBoolean("pause", false);
            editor.apply();
            player_play.setImageResource(R.mipmap.player_pause);
            Intent m_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
            m_intent.putExtra("main_song", song);
            m_intent.putStringArrayListExtra("playlist", playallList);
            m_intent.putExtra("player", "Play");
            ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, m_intent);
        }

//        player_play.setImageResource(R.mipmap.player_pause);
//        Intent m_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
//        m_intent.putExtra("player", "Resume");
//        startService(m_intent);
//        if (isMyServiceRunning(NatureSoundService.class)) {
//            Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class);
//            n_intent.putExtra("player", "Resume");
//            startService(n_intent);
//        }

        Log.e("reume", "resume______");

        LocalBroadcastManager.getInstance(CreativityAffirmationActivityNew.this).registerReceiver(
                MReceiver, new IntentFilter("GUI_UPDATE_ACTION"));

        Intent mintent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
        mintent.putExtra("player", "ACTION_SEND_INFO");
        ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, mintent);

        LocalBroadcastManager.getInstance(CreativityAffirmationActivityNew.this).registerReceiver(
                NReceiver, new IntentFilter("VOICE_UPDATE_ACTION"));

    }

    @Override
    protected void onDestroy() {
//        stopService(new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class));
//        stopService(new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class));
        audioManager.setStreamVolume(AudioManager.STREAM_ALARM, default_volume, 0);
        if (isMyServiceRunning(BackgroundSoundService.class)) {
            stopService(new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class));
        }
        if (isMyServiceRunning(NatureSoundService.class)) {
            stopService(new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class));
        }
        super.onDestroy();
//        if (MReceiver != null) {
//            unregisterReceiver(MReceiver);
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
//        stopService(new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class));
//        stopService(new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class));
//        audioManager.setStreamVolume(AudioManager.STREAM_ALARM, default_volume, 0);
//        SharedPreferences preferences = getApplicationContext().getSharedPreferences("player_state",0);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putBoolean("pause",true);
//        editor.apply();
//        player_play.setImageResource(R.mipmap.player_sound_play);
//        Intent m_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
//        m_intent.putExtra("player", "Pause");
//        startService(m_intent);
//        if (isMyServiceRunning(NatureSoundService.class)) {
//            Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, NatureSoundService.class);
//            n_intent.putExtra("player", "Pause");
//            startService(n_intent);
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Intent n_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
//        n_intent.putExtra("player", "Stop_notification");
//        ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, n_intent);
//        if (MReceiver != null) {
//            unregisterReceiver(MReceiver);
//        }
    }

    @Override
    public void onBackPressed() {
        if (isMyServiceRunning(BackgroundSoundService.class)) {
            if (!back) {
                super.onBackPressed();
                try {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean("playing", playing);
                    editor.apply();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void pause() {
        player_play.setImageResource(R.mipmap.player_sound_play);
        Log.e("switch", "d");
    }

    public static void start() {
        player_play.setImageResource(R.mipmap.player_pause);
        Log.e("switch", "d");
    }
    //

    private BroadcastReceiver MReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            current_time = intent.getIntExtra("ACTUAL_TIME_VALUE_EXTRA", 0);
            total_duration = intent.getIntExtra("TOTAL_TIME_VALUE_EXTRA", 0);
//            Log.e("time", String.valueOf(intent.getIntExtra("ACTUAL_TIME_VALUE_EXTRA", 200)));
//            Log.e("duration", String.valueOf(intent.getIntExtra("TOTAL_TIME_VALUE_EXTRA", 200)));
            String time = getTimeString(current_time);
            Log.e("switcha", String.valueOf(current_time));
            if (circularSeekBar != null) {
                circularSeekBar.setProgress(current_time);
                circularSeekBar.setMax(total_duration);
            }
            if (player_timer != null) {
                player_timer.setText(time);
            }
        }
    };

    private BroadcastReceiver NReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            volume = intent.getIntExtra("Volume", 50);
//            Log.e("time", String.valueOf(intent.getIntExtra("ACTUAL_TIME_VALUE_EXTRA", 200)));
//            Log.e("duration", String.valueOf(intent.getIntExtra("TOTAL_TIME_VALUE_EXTRA", 200)));
            if (player_vol_bar != null) {
                player_vol_bar.setProgress(volume);
                player_vol_bar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM));
            }
        }
    };

    public void getNatureData(String user_Id, String session_id) {
        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<MusicPlayerResponse> call = apiInterface.getMusicResponse(user_Id, session_id);

        call.enqueue(new Callback<MusicPlayerResponse>() {
            @Override
            public void onResponse(Call<MusicPlayerResponse> call, Response<MusicPlayerResponse> response) {
                if (response.isSuccessful()) {

                    MusicPlayerResponse resource = response.body();
                    if (resource.getSuccess()) {
                        if (myRecording) {
                            spinner_voice.setVisibility(View.GONE);
                            name.setVisibility(View.VISIBLE);
                            name.setText(nametxt);
                            Picasso.get().load(profileImg).into(voice_img);
                            playSong();
                        } else if (type.equals("affirmation")) {
                            voices = resource.getData().getGetVoice();
                            for (int a = 0; a < voices.size(); a++) {
                                voiceList.add(voices.get(a).getName());
                            }
                            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(CreativityAffirmationActivityNew.this, R.layout.support_simple_spinner_dropdown_item, voiceList);
                            spinner_voice.setAdapter(arrayAdapter);
                            for (int a = 0; a < voices.size(); a++) {
                                if (voice_id.equals(String.valueOf(voices.get(a).getId()))) {
                                    spinner_voice.setSelection(a);
                                }
                            }
                        }
//                        else if (myFavourite){
//                            spinner_voice.setVisibility(View.GONE);
//                            name.setVisibility(View.VISIBLE);
//                            name.setText(nametxt);
//                            Picasso.get().load(profileImg).into(voice_img);
//                            playSong();
//                        }
                        PlayerNatureAdapter adapter = new PlayerNatureAdapter(CreativityAffirmationActivityNew.this, resource.getData().getNature());
                        nature_recycler.setAdapter(adapter);

                    } else {
                        Toast.makeText(CreativityAffirmationActivityNew.this, "" + resource.getMessages(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(CreativityAffirmationActivityNew.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MusicPlayerResponse> call, Throwable t) {
                Toast.makeText(CreativityAffirmationActivityNew.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = voices.get(position).getName();
        Picasso.get().load(voices.get(position).getImage()).into(voice_img);
        playSong(voices.get(position).getSongs());
//        Toast.makeText(parent.getContext(), item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void checkVoice(String userID) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<CheckVoiceResponse> call = apiInterface.checkVoice(userID);
        call.enqueue(new Callback<CheckVoiceResponse>() {
            @Override
            public void onResponse(Call<CheckVoiceResponse> call, Response<CheckVoiceResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        checkVoiceResponse = response.body();
                        assert checkVoiceResponse != null;
                        if (checkVoiceResponse.getSuccess()) {
                            if (!checkVoiceResponse.getData().isEmpty()) {
                                voice_id = checkVoiceResponse.getData().get(0).getVoiceId();
                                getNatureData(userID, sessionId);
                            } else {
                                Toast.makeText(CreativityAffirmationActivityNew.this, "Server Error", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        } else {
                            Toast.makeText(CreativityAffirmationActivityNew.this, "" + checkVoiceResponse.getMessages(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(CreativityAffirmationActivityNew.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckVoiceResponse> call, Throwable t) {
                Toast.makeText(CreativityAffirmationActivityNew.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void favouriteSwitch(String userID, String session_Id) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<GetFavouriteSession> call = apiInterface.getFavouriteSession(userID, session_Id);
        call.enqueue(new Callback<GetFavouriteSession>() {
            @Override
            public void onResponse(Call<GetFavouriteSession> call, Response<GetFavouriteSession> response) {
                if (response.isSuccessful()) {
                    GetFavouriteSession getFavouriteSession = response.body();
                    if (getFavouriteSession.getSuccess()) {
                        fav_session = getFavouriteSession.getData().getFavourite().get(0).getFavouriteStatus().toString();
                        if (fav_session.equals("0")) {
                            img_heart.setVisibility(View.VISIBLE);
                            img_heart.setImageResource(R.mipmap.gray_heart_empty);
                        } else if (fav_session.equals("1")) {
                            img_heart.setVisibility(View.VISIBLE);
                            img_heart.setImageResource(R.mipmap.shaed_heart_pink);
                        }
                    }
                } else {
                    Toast.makeText(CreativityAffirmationActivityNew.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetFavouriteSession> call, Throwable t) {
                Toast.makeText(CreativityAffirmationActivityNew.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void playSong() {
        player_play.setImageResource(R.mipmap.player_pause);
        Intent m_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
        m_intent.putExtra("main_song", song);
        m_intent.putStringArrayListExtra("playlist", playallList);
        m_intent.putExtra("player", "Play");
        m_intent.putExtra("song_name", song_txt);
        ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, m_intent);
        playing = true;
    }

    public void playSong(String voice) {
        player_play.setImageResource(R.mipmap.player_pause);
        Intent m_intent = new Intent(CreativityAffirmationActivityNew.this, BackgroundSoundService.class);
        m_intent.putExtra("main_song", voice);
        m_intent.putStringArrayListExtra("playlist", playallList);
        m_intent.putExtra("player", "Play");
        m_intent.putExtra("song_name", song_txt);
        ContextCompat.startForegroundService(CreativityAffirmationActivityNew.this, m_intent);
        playing = true;
    }


}
