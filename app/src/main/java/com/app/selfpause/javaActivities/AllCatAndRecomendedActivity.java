package com.app.selfpause.javaActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.AllCatModelClasses.CategoryDataModelClass;
import com.app.selfpause.ModelClasses.AllCatModelClasses.GetCategoryAndRecomendedModelClass;
import com.app.selfpause.ModelClasses.AllCatModelClasses.RecomandedModelClass;
import com.app.selfpause.R;
import com.app.selfpause.adapter.AllCategoryAdapter;
import com.app.selfpause.adapter.MusicAdapter;
import com.app.selfpause.adapter.RecomendedAdapter;
import com.app.selfpause.adapter.SoundScapeAdapter;
import com.app.selfpause.utilityClasses.BackgroundSoundService;
import com.app.selfpause.utilityClasses.NatureSoundService;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllCatAndRecomendedActivity extends BaseActivity {

    private RecyclerView categoryRV, recomendedRV;
    ApiInterface apiInterface;
    List<CategoryDataModelClass> categoryDataModelClasses;
    List<RecomandedModelClass> recomandedModelClasses;
    String cat_ID;
    AppCompatImageView abundance, health, title_weight_img, ll_wl_my_recordings_img;
    ImageView img_back_two;
    String userID;
    GetCategoryAndRecomendedModelClass resource;
    String mypreference = "mypref", user_id = "user_id";

    private RelativeLayout titleLL, my_recordings_ll;
    private CustomBoldtextView dummyText, recomended_txt, weight_title;
    private ProgressBar weight_progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weight_two_fragment);

        categoryRV = findViewById(R.id.allCat_listRV);
        recomendedRV = findViewById(R.id.recomended_RV);
        abundance = findViewById(R.id.abundance);
        health = findViewById(R.id.health);

        titleLL = findViewById(R.id.title_weight_ll);
        title_weight_img = findViewById(R.id.title_weight_img);
        dummyText = findViewById(R.id.titleBelow_txt);
        weight_progressBar = findViewById(R.id.weight_progressBar);
        my_recordings_ll = findViewById(R.id.ll_wl_my_recordings);
        recomended_txt = findViewById(R.id.recomended_txt);
        weight_title = findViewById(R.id.weight_title);
        img_back_two = findViewById(R.id.img_back_two);
        ll_wl_my_recordings_img = findViewById(R.id.ll_wl_my_recordings_img);

        titleLL.setVisibility(View.INVISIBLE);
        dummyText.setVisibility(View.INVISIBLE);
        my_recordings_ll.setVisibility(View.INVISIBLE);
        recomended_txt.setVisibility(View.INVISIBLE);
        categoryRV.setVisibility(View.INVISIBLE);
//        recomendedRV.setVisibility(View.INVISIBLE);

        SharedPreferences pref = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = pref.getString(user_id, "");

        cat_ID = getIntent().getStringExtra("cat_id");
        Log.e("CATrert_ID", cat_ID);

        img_back_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        health.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllCatAndRecomendedActivity.this, AllCatAndRecomendedActivity.class);
                intent.putExtra("cat_id", "58");
                startActivity(intent);
            }
        });

        abundance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllCatAndRecomendedActivity.this, AllCatAndRecomendedActivity.class);
                intent.putExtra("cat_id", "69");
                startActivity(intent);
            }
        });

        my_recordings_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllCatAndRecomendedActivity.this, WeightActivityNew.class);
                intent.putExtra("category_id", cat_ID);
                intent.putExtra("title", resource.getData().getAffirmation().get(0).getName());
                startActivity(intent);
            }
        });

        GridLayoutManager grid_sound = new GridLayoutManager(AllCatAndRecomendedActivity.this, 3, RecyclerView.VERTICAL, false);
        categoryRV.setLayoutManager(grid_sound);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AllCatAndRecomendedActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recomendedRV.setLayoutManager(linearLayoutManager);

        getData(userID, cat_ID);

    }

    public void getData(String userID, String cat_ID) {

        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<GetCategoryAndRecomendedModelClass> call = apiInterface.getCatAndRecomended(userID, cat_ID);

        call.enqueue(new Callback<GetCategoryAndRecomendedModelClass>() {
            @Override
            public void onResponse(Call<GetCategoryAndRecomendedModelClass> call, Response<GetCategoryAndRecomendedModelClass> response) {

                if (response.isSuccessful()) {

                    resource = response.body();

                    assert resource != null;
                    if (resource.getSuccess()) {

                        titleLL.setVisibility(View.VISIBLE);
                        dummyText.setVisibility(View.VISIBLE);
                        my_recordings_ll.setVisibility(View.VISIBLE);
                        recomended_txt.setVisibility(View.VISIBLE);
                        categoryRV.setVisibility(View.VISIBLE);
//                        recomendedRV.setVisibility(View.VISIBLE);
                        weight_progressBar.setVisibility(View.GONE);

                        categoryDataModelClasses = resource.getData().getAffirmation();
                        recomandedModelClasses = resource.getData().getRecomended();

                        weight_title.setText(resource.getData().getAffirmation().get(0).getName());
                        dummyText.setText(resource.getData().getAffirmation().get(0).getDescription());
                        Picasso.get().load(resource.getData().getAffirmation().get(0).getImage()).into(title_weight_img);
                        Picasso.get().load(resource.getData().getAffirmation().get(0).getcircular_images()).into(ll_wl_my_recordings_img);

                        AllCategoryAdapter allCategoryAdapter = new AllCategoryAdapter(AllCatAndRecomendedActivity.this, resource.getData().getSession(), resource.getData().getAffirmation().get(0).getName());
                        categoryRV.setAdapter(allCategoryAdapter);

//                        final RecomendedAdapter recomendedAdapter = new RecomendedAdapter(AllCatAndRecomendedActivity.this, resource.getData().getRecomended());
//                        recomendedRV.setAdapter(recomendedAdapter);

                    } else {
                        Toast.makeText(AllCatAndRecomendedActivity.this, "" + resource.getMessages(), Toast.LENGTH_SHORT).show();
                        weight_progressBar.setVisibility(View.GONE);
                    }

                } else {
                    Toast.makeText(AllCatAndRecomendedActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                    weight_progressBar.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<GetCategoryAndRecomendedModelClass> call, Throwable t) {
                Toast.makeText(AllCatAndRecomendedActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                weight_progressBar.setVisibility(View.GONE);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData(userID, cat_ID);
    }

    @Override
    protected void onDestroy() {
        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent n_intent = new Intent(AllCatAndRecomendedActivity.this, BackgroundSoundService.class);
//            n_intent.putExtra("player", "Stop_notification");
//            ContextCompat.startForegroundService(AllCatAndRecomendedActivity.this, n_intent);
            stopService(new Intent(AllCatAndRecomendedActivity.this,BackgroundSoundService.class));
        }
        if (isMyServiceRunning(NatureSoundService.class)) {
            stopService(new Intent(AllCatAndRecomendedActivity.this,NatureSoundService.class));
        }
        super.onDestroy();
//        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent m_intent = new Intent(AllCatAndRecomendedActivity.this, BackgroundSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(AllCatAndRecomendedActivity.this, m_intent);
//        }
//        if (isMyServiceRunning(NatureSoundService.class)) {
//            Intent m_intent = new Intent(AllCatAndRecomendedActivity.this, NatureSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(AllCatAndRecomendedActivity.this, m_intent);
//        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
