package com.app.selfpause.Custom_Widgets;

/*public class CustomItalictextView {
}*/


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

@SuppressLint("AppCompatCustomView")
public class CustomBoldtextView extends AppCompatTextView {

    public CustomBoldtextView(Context context) {
        super(context);
        init(context);
    }

    public CustomBoldtextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomBoldtextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    public void init(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "font/Comfortaa_Bold.ttf");
        setTypeface(tf, Typeface.BOLD);
        // setTextColor(Color.WHITE);
    }
}
