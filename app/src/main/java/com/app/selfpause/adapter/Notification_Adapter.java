package com.app.selfpause.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.ModelClasses.NotificationData;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.NotificationActivity;

import java.util.List;

public class Notification_Adapter extends RecyclerView.Adapter<Notification_Adapter.itemView> {

    Context context;
    List<NotificationData> data;

    public Notification_Adapter(Context context, List<NotificationData> data) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public itemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notifications, parent, false);
        return new Notification_Adapter.itemView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull itemView holder, int position) {
        holder.txt_title.setText(data.get(position).getTitle());
        holder.txt_info.setText(data.get(position).getBody());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class itemView extends RecyclerView.ViewHolder {

        TextView txt_title, txt_info;

        public itemView(@NonNull View itemView) {
            super(itemView);

            txt_title = itemView.findViewById(R.id.txt_title);
            txt_info = itemView.findViewById(R.id.txt_info);

        }
    }
}
