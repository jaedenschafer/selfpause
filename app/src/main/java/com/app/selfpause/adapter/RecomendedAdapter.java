package com.app.selfpause.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.ModelClasses.AllCatModelClasses.CategoryDataModelClass;
import com.app.selfpause.ModelClasses.AllCatModelClasses.RecomandedModelClass;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.AllCatAndRecomendedActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecomendedAdapter extends RecyclerView.Adapter<RecomendedAdapter.MyViewHolder> {

    Context context;
    List<RecomandedModelClass> recomandedModelClasses;

    public RecomendedAdapter(Context context, List<RecomandedModelClass> recomandedModelClasses) {
        this.context = context;
        this.recomandedModelClasses = recomandedModelClasses;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recomended_layout_rv, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        final RecomandedModelClass recomandedModelClass = recomandedModelClasses.get(position);

//        Picasso.get().load(recomandedModelClass.getBanner()).into(holder.backImageView);
        if (position == 0) {
            holder.backImageView.setImageResource(R.drawable.abundance_recommended);
        } else if (position == 1) {
            holder.backImageView.setImageResource(R.drawable.health_recommended);
        }

        holder.backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == 1) {
                    Intent intent = new Intent(context, AllCatAndRecomendedActivity.class);
                    intent.putExtra("cat_id", "58");
                    holder.itemView.getContext().startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return recomandedModelClasses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView backImageView, lockImageView;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            backImageView = itemView.findViewById(R.id.img_abun);
//            lockImageView=itemView.findViewById(R.id.sound_scape_back_lock_IV);


        }
    }
}
