package com.app.selfpause.adapter.FavoriteAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.FavoriteModelClass.FavoritesModelClass;
import com.app.selfpause.ModelClasses.FavoriteModelClass.SubFavoritesModelClass;
import com.app.selfpause.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FavoritesCategoryAdapter extends RecyclerView.Adapter<FavoritesCategoryAdapter.MyViewHolder> {

    Context context;
    List<FavoritesModelClass> favoritesModelClasses;
    List<SubFavoritesModelClass> subFavoritesModelClasses;
    Boolean check;


    public FavoritesCategoryAdapter(Context context, List<FavoritesModelClass> data) {
        this.context=context;
        this.favoritesModelClasses = data;
//        this.subFavoritesModelClasses = subFavoritesModelClasses;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.favorites_catageory_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final FavoritesModelClass favoritesModelClass = favoritesModelClasses.get(position);
        holder.cat_textView.setText(favoritesModelClass.getName());
        Picasso.get().load(favoritesModelClass.getImage()).into(holder.backImageView);

        check = favoritesModelClass.isSelected();

        holder.backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
                holder.categoryList.setLayoutManager(linearLayoutManager);
                SubCategoryAdapter subCategoryAdapter = new SubCategoryAdapter(context,favoritesModelClasses.get(position).getSession(),favoritesModelClass.getName());
                holder.categoryList.setAdapter(subCategoryAdapter);
                holder.categoryList.setNestedScrollingEnabled(false);

                if (check){
                    check = false;
                    holder.categoryList.setVisibility(View.VISIBLE);
                }
                else {
                    check = true;
                    holder.categoryList.setVisibility(View.GONE);
                }

            }


        });


    }

    @Override
    public int getItemCount() {
        return favoritesModelClasses.size();
//
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView backImageView;
        CustomBoldtextView cat_textView;
        RelativeLayout backRL;
        RecyclerView categoryList;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

//            backRL=itemView.findViewById(R.id.backFavView_RL);
            categoryList = itemView.findViewById(R.id.list_RV);
            backImageView=itemView.findViewById(R.id.recyclerListCat_back_FavImg);
            cat_textView=itemView.findViewById(R.id.recyclerListCat_FavTextView);

        }
    }
}
