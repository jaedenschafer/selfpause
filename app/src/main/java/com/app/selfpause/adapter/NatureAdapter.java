package com.app.selfpause.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.ModelClasses.NatureData;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.CreativityAffirmationActivityNew;
import com.app.selfpause.javaActivities.GetMorePaymentActivity;
import com.app.selfpause.javaActivities.SubscriptionActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NatureAdapter extends RecyclerView.Adapter<NatureAdapter.itemHolder> {
    Context context;
    List<NatureData> nature;

    public NatureAdapter(Context context, List<NatureData> nature) {
        this.context = context;
        this.nature = nature;
    }

    @NonNull
    @Override
    public itemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nature_recycleritem, parent, false);
        return new itemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final itemHolder holder, final int position) {
        Picasso.get().load(nature.get(position).getImages()).into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (nature.getData().getSoundScopes().get(position).getLockUnlockStatus().equals(0)){
//
//                    Intent intent = new Intent(getActivity(), GetMorePaymentActivity.class);
//                    intent.putExtra("song", natureMusic);
//                    intent.putExtra("nature_id", nature_id);
//                    intent.putExtra("nature_name", nature_name);
//                    startActivity(intent);
//                }
//                else if (resource.getData().getSoundScopes().get(position).getLockUnlockStatus().equals(1)){
//
//                    Intent intent = new Intent(getActivity(), CreativityAffirmationActivityNew.class);
//                    intent.putExtra("song", natureMusic);
//                    intent.putExtra("nature_id", nature_id);
//                    intent.putExtra("nature_name", nature_name);
//                    startActivity(intent);
//                }

                if (nature.get(position).getLockUnlockStatus().equals(0)) {
                    Intent intent = new Intent(context, SubscriptionActivity.class);
                    intent.putExtra("price", nature.get(position).getPrice().toString());
                    intent.putExtra("song", nature.get(position).getSongs());
                    intent.putExtra("nature_id", nature.get(position).getNatureId().toString());
                    intent.putExtra("nature_name", nature.get(position).getNatureName());
                    intent.putExtra("screen", "1");
                    holder.itemView.getContext().startActivity(intent);
                } else if (nature.get(position).getLockUnlockStatus().equals(1)) {
                    Intent intent = new Intent(context, CreativityAffirmationActivityNew.class);
                    intent.putExtra("song", nature.get(position).getSongs());
                    intent.putExtra("category", nature.get(position).getMusicType().toString());
                    intent.putExtra("song_name", nature.get(position).getNatureName().toString());
                    intent.putExtra("type","nature");
                    holder.itemView.getContext().startActivity(intent);
                }

            }
        });
        Log.e("songs", nature.get(position).getSongs());
    }

    @Override
    public int getItemCount() {
        return nature.size();
    }

    public class itemHolder extends RecyclerView.ViewHolder {

        AppCompatImageView image;

        public itemHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.nature_lib_drop);
        }
    }
}
