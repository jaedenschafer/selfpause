package com.app.selfpause.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.ModelClasses.AllCatModelClasses.CategoryAndRecomendedData;
import com.app.selfpause.ModelClasses.AllCatModelClasses.CategoryDataModelClass;
import com.app.selfpause.ModelClasses.SessionModelClass;
import com.app.selfpause.ModelClasses.SoundModel.SoundScapeModelClass;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.CreativityAffirmationActivityNew;
import com.app.selfpause.javaActivities.GetMorePaymentActivity;
import com.app.selfpause.javaActivities.SubscriptionActivity;
import com.app.selfpause.javaActivities.WeightActivityNew;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllCategoryAdapter extends RecyclerView.Adapter<AllCategoryAdapter.MyViewHolder> {

    Context context;
    List<SessionModelClass> sessionModelClasses;
    String name;

    public AllCategoryAdapter(Context context, List<SessionModelClass> sessionModelClasses, String name) {
        this.context = context;
        this.sessionModelClasses = sessionModelClasses;
        this.name = name;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.session_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        final SessionModelClass sessionModelClasse = sessionModelClasses.get(position);

        Picasso.get().load(sessionModelClasse.getImages()).into(holder.backImageView);
        holder.session_TV.setText(sessionModelClasse.getName());

        holder.backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Intent intent = new Intent(context, CreativityAffirmationActivityNew.class);
//                intent.putExtra("song", sessionModelClasse.getSongs());
//                intent.putExtra("song_name", sessionModelClasse.getName());
//                intent.putExtra("category", name);
//                holder.itemView.getContext().startActivity(intent);
                if (sessionModelClasse.getLockUnlockStatus().equals(0)) {
                    Intent intent = new Intent(context, SubscriptionActivity.class);
                    intent.putExtra("price", sessionModelClasse.getPrice().toString());
//                    intent.putExtra("song", nature);
                    intent.putExtra("nature_id", sessionModelClasse.getNatureId().toString());
                    intent.putExtra("nature_name", sessionModelClasse.getName().toString());
                    intent.putExtra("screen", "2");
                    context.startActivity(intent);
                } else if (sessionModelClasse.getLockUnlockStatus().equals(1)) {
                    Intent intent = new Intent(context, CreativityAffirmationActivityNew.class);
                    intent.putExtra("song", sessionModelClasse.getSongs());
                    intent.putExtra("song_name", sessionModelClasse.getName());
                    intent.putExtra("category", name);
                    intent.putExtra("fav_session",sessionModelClasse.getFavourite().toString());
                    intent.putExtra("type","affirmation");
                    intent.putExtra("session_id",String.valueOf(sessionModelClasse.getNatureId()));
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return sessionModelClasses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView backImageView;
        TextView session_TV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            backImageView = itemView.findViewById(R.id.session_IV);
            session_TV = itemView.findViewById(R.id.session_TV);
//            lockImageView=itemView.findViewById(R.id.sound_scape_back_lock_IV);

        }
    }
}
