package com.app.selfpause.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.ModelClasses.SoundModel.MusicModelClass;
import com.app.selfpause.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MyViewHolder> {

    Context context;
    List<MusicModelClass> musicModelClasses;

    public MusicAdapter(Context context, List<MusicModelClass> musicModelClasses) {
        this.context = context;
        this.musicModelClasses = musicModelClasses;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.music_layout_rv, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        final MusicModelClass musicModelClass = musicModelClasses.get(position);

        Picasso.get().load(musicModelClass.getImages()).into(holder.backImageView);

//        holder.backImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(context, GetMore_Activity.class);
//                context.startActivity(intent);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return musicModelClasses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView backImageView, lockImageView;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            backImageView = itemView.findViewById(R.id.music_back_IV);
//            lockImageView=itemView.findViewById(R.id.sound_scape_back_lock_IV);


        }
    }
}
