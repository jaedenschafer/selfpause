package com.app.selfpause.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.RecordingCategory;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.AllCatAndRecomendedActivity;
import com.app.selfpause.javaActivities.CreativityAffirmationActivityNew;
import com.app.selfpause.javaActivities.WeightActivityNew;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecordcCategoryAdapter extends RecyclerView.Adapter<RecordcCategoryAdapter.itemHolder> {
    Context context;
    List<RecordingCategory> category;

    public static int a =0;

    public RecordcCategoryAdapter(Context context, List<RecordingCategory> category) {
        this.context = context;
        this.category = category;
    }

    @NonNull
    @Override
    public itemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recording_recycleritem, parent, false);
        return new itemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final itemHolder holder, final int position) {
        Picasso.get().load(category.get(position).getImage()).into(holder.image);
        holder.title.setText(category.get(position).getName());

//        if (category.get(position).getMyRecording() == null) {
//            holder.image.setVisibility(View.GONE);
//            holder.category_playall.setVisibility(View.GONE);
//        }
//        if (category.get(position).getCount()==0){
//            holder.image.setVisibility(View.GONE);
//            holder.category_playall.setVisibility(View.GONE);
//        }

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer cat = category.get(position).getId();
                String cat_id = String.valueOf(cat);
                Log.e("CAT_ID", cat_id);

                Intent intent = new Intent(context, WeightActivityNew.class);
                intent.putExtra("category_id", cat_id);
                intent.putExtra("title", category.get(position).getName());
                holder.itemView.getContext().startActivity(intent);
            }
        });

        holder.category_playall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category.get(position).getCount() != 0) {
                    Intent intent = new Intent(context, CreativityAffirmationActivityNew.class);
//                intent.putExtra("demo","https://clientstagingdev.com/meditation/public/voice/1586425636.mp3");
                    intent.putExtra("myRecording", true);
                    intent.putExtra("category", category.get(position).getName());
                    intent.putStringArrayListExtra("playlist", (ArrayList<String>) category.get(position).getMyRecording());
                    holder.itemView.getContext().startActivity(intent);
                }else {
                    Toast.makeText(context, "No Recordings Found", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return category.size();
    }

    public class itemHolder extends RecyclerView.ViewHolder {

        AppCompatImageView image;
        CustomBoldtextView title;
        ImageView category_playall;

        public itemHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.category_img);
            title = itemView.findViewById(R.id.category_title);
            category_playall = itemView.findViewById(R.id.category_playall);
        }
    }
}
