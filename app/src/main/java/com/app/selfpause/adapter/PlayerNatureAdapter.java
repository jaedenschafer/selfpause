package com.app.selfpause.adapter;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.ModelClasses.MusicPlayerNature;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.CreativityAffirmationActivityNew;
import com.app.selfpause.utilityClasses.NatureSoundService;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PlayerNatureAdapter extends RecyclerView.Adapter<PlayerNatureAdapter.itemView> {

    Context context;
    List<MusicPlayerNature> nature;
    public static Dialog nature_dialog;

    public PlayerNatureAdapter(Context context, List<MusicPlayerNature> nature) {
        this.context = context;
        this.nature = nature;
    }

    @NonNull
    @Override
    public itemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.playe_nature_item, parent, false);
        return new itemView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final itemView holder, int position) {
        Picasso.get().load(nature.get(position).getIcon()).into(holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreativityAffirmationActivityNew.img_vol_bar.setVisibility(View.VISIBLE);
                if (isMyServiceRunning(NatureSoundService.class)) {
                    holder.itemView.getContext().stopService(new Intent(context, NatureSoundService.class));
                } else {
                    Intent n_intent = new Intent(context, NatureSoundService.class);
                    n_intent.putExtra("nature_song", nature.get(position).getSongs());
                    n_intent.putExtra("player", "Play");
                    holder.itemView.getContext().startService(n_intent);

                    CreativityAffirmationActivityNew.nature_loading.setVisibility(View.VISIBLE);
//                    holder.natureitem_rl.setVisibility(View.VISIBLE);

//                    nature_dialog = new Dialog(context, R.style.CustomDialogTheme);
//                    nature_dialog.setContentView(R.layout.dialog_loading);
//                    Window window = nature_dialog.getWindow();
//                    window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//                    nature_dialog.show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return nature.size();
    }

    public static class itemView extends RecyclerView.ViewHolder {

        AppCompatImageView image;
//        RelativeLayout natureitem_rl;

        public itemView(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.img_one);
//            natureitem_rl = itemView.findViewById(R.id.natureitem_rl);

        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
