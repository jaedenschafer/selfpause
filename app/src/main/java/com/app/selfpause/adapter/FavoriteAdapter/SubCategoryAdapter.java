package com.app.selfpause.adapter.FavoriteAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.FavoriteModelClass.FavoritesModelClass;
import com.app.selfpause.ModelClasses.FavoriteModelClass.SubFavoritesModelClass;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.CreativityAffirmationActivityNew;
import com.app.selfpause.javaActivities.FavoritesActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder> {

    Context context;
    List<SubFavoritesModelClass> subFavoritesModelClasses;
    String name;

    //    public SubCategoryAdapter(Context context, List<SubFavoritesModelClass> subFavoritesModelClasses) {
//        this.context=context;
//        this.subFavoritesModelClasses = subFavoritesModelClasses;
//    }
    public SubCategoryAdapter(Context context, List<SubFavoritesModelClass> subFavoritesModelClasses, String name) {
        this.context = context;
        this.subFavoritesModelClasses = subFavoritesModelClasses;
        name = this.name;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_sub_fav_cat_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final SubFavoritesModelClass subFavoritesModelClass = subFavoritesModelClasses.get(position);
        holder.cat_textView.setText(subFavoritesModelClass.getAffirmationTitle());
//        holder.cat_textView.setText("Health Body Image Affirmation affirmationas affirmation");
        holder.cat_textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CreativityAffirmationActivityNew.class);
//                intent.putExtra("demo","https://clientstagingdev.com/meditation/public/voice/1586425636.mp3");
//                                    intent.putExtra("song", sessionModelClasse.getSongs());
                intent.putExtra("song_name", subFavoritesModelClass.getAffirmationTitle());
                intent.putExtra("category", name);
                intent.putExtra("fav_session", String.valueOf(subFavoritesModelClass.getFavouriteStatus()));
                intent.putExtra("type", "affirmation");
                intent.putExtra("session_id", String.valueOf(subFavoritesModelClass.getId()));
//                intent.putExtra("song", subFavoritesModelClass.getSongs());
                holder.itemView.getContext().startActivity(intent);
            }
        });
//        Picasso.get().load(subFavoritesModelClass.getFavriteStatus()).into(holder.hurtIV);
//        Picasso.get().load(subFavoritesModelClass.getSongsTitle()).into(holder.songIV);

    }

    @Override
    public int getItemCount() {
        return subFavoritesModelClasses.size();
//
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView hurtIV, songIV;
        CustomBoldtextView cat_textView;
        RelativeLayout backRL;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            hurtIV = itemView.findViewById(R.id.hurt_IV);
//            songIV = itemView.findViewById(R.id.recyclerListCat_playIV);
            cat_textView = itemView.findViewById(R.id.recyclerListCat_playTV);

        }
    }
}
