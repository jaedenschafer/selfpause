package com.app.selfpause.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.CategoriesModelClass;
import com.app.selfpause.ModelClasses.InterestedData;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.CategoriesActivities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {

    List<CategoriesModelClass> categoriesModelClasses = new ArrayList<>();
    Context context;
    List<InterestedData> interestedData;

    public CategoriesAdapter(List<CategoriesModelClass> categoriesModelClasses, Context context, List<InterestedData> interestedData) {
        this.categoriesModelClasses = categoriesModelClasses;
        this.context=context;
        this.interestedData = interestedData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.categroies_recycler_list,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final CategoriesModelClass categoriesModelClass = categoriesModelClasses.get(position);
        holder.cat_textView.setText(categoriesModelClasses.get(position).getName());
        Picasso.get().load(categoriesModelClasses.get(position).getImage()).into(holder.backImageView);

        if (interestedData != null) {
            for (int i = 0; i <= interestedData.size() - 1; i++) {
                if (interestedData.get(i).getId().equals(categoriesModelClasses.get(position).getId())) {
                    categoriesModelClass.setSelected(!categoriesModelClass.isSelected());
                    holder.backRL.setBackgroundResource(categoriesModelClass.isSelected() ? R.drawable.categories_bg : R.drawable.categories_bg_white);
                    CategoriesActivities.data.add(categoriesModelClasses.get(position).getId());
                }
            }
        }
         holder.backImageView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

//                 categoriesModelClasses.get(position).setSelected(true);

                 categoriesModelClass.setSelected(!categoriesModelClass.isSelected());
                 holder.backRL.setBackgroundResource(categoriesModelClass.isSelected() ? R.drawable.categories_bg : R.drawable.categories_bg_white);
             }
         });


    }

    @Override
    public int getItemCount() {
        return categoriesModelClasses == null ? 0 : categoriesModelClasses.size();
//
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView backImageView;
        CustomBoldtextView cat_textView;
        RelativeLayout backRL;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            backRL=itemView.findViewById(R.id.backView_RL);
            backImageView=itemView.findViewById(R.id.recyclerListCat_back_img);
            cat_textView=itemView.findViewById(R.id.recyclerListCat_TextView);


        }
    }
}

//      data.add(String.valueOf(categoriesModelClasses.get(position).getId()));
//              Toast.makeText(context, ""+data, Toast.LENGTH_SHORT).show();
//
//
//              for(int i=0; i<data.size(); i++){
//        if(holder.backRL.equals(data.get(i))){
//        data.remove(i);
//        return;
//        }
//        }
