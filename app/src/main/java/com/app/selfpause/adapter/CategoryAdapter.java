package com.app.selfpause.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.CategoryData;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.AllCatAndRecomendedActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.itemHolder> implements Filterable {

    Context context;
    List<CategoryData> categoryData;
    ArrayList<CategoryData> list_data;
    private ArrayList<CategoryData> mDisplayedValues;

    public CategoryAdapter(Context context, List<CategoryData> categoryData) {
        this.context = context;
        this.categoryData = categoryData;
        list_data = (ArrayList<CategoryData>) categoryData;
        this.mDisplayedValues = new ArrayList<CategoryData>();
        this.mDisplayedValues.addAll(list_data);

    }

    @NonNull
    @Override
    public itemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.interested_recycleritem, parent, false);
        return new itemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final itemHolder holder, final int position) {
        Picasso.get().load(list_data.get(position).getImage()).into(holder.image);
        holder.name.setText(list_data.get(position).getName());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, WeighTwoActivity.class);

                Integer cat = list_data.get(position).getId();
                String cat_id = String.valueOf(cat);
                Log.e("CAT_ID", cat_id);

                Intent intent = new Intent(context, AllCatAndRecomendedActivity.class);
                intent.putExtra("cat_id", cat_id);
                holder.itemView.getContext().startActivity(intent);


            }
        });
    }

    //filter method
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    categoryData = categoryData;

                } else {
                    List filteredList = new ArrayList<>();
                    for (CategoryData row : categoryData) {


                        //change this to filter according to your case
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    categoryData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categoryData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                categoryData = (ArrayList) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }


    @Override
    public int getItemCount() {
        return list_data.size();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());

        list_data.clear();
        if (charText.length() == 0) {

            list_data.addAll(mDisplayedValues);
        } else {

            for (int i = 0; i < mDisplayedValues.size(); i++) {

                if (mDisplayedValues.get(i).getName()
                        .toLowerCase(Locale.getDefault())
                        .contains(charText)) {

                    list_data.add(mDisplayedValues.get(i));

                }
            }
        }
        notifyDataSetChanged();
    }

    public class itemHolder extends RecyclerView.ViewHolder {

        AppCompatImageView image;
        CustomBoldtextView name;

        public itemHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.interested_recyclerimage);
            name = itemView.findViewById(R.id.interested_recyclername);
        }
    }


}
