package com.app.selfpause.JavaFragment;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.SoundModel.GetSoundAndScapeResponse;
import com.app.selfpause.ModelClasses.SoundModel.MusicModelClass;
import com.app.selfpause.ModelClasses.SoundModel.SoundScapeModelClass;
import com.app.selfpause.R;
import com.app.selfpause.adapter.MusicAdapter;
import com.app.selfpause.adapter.SoundScapeAdapter;
import com.app.selfpause.javaActivities.AllCatAndRecomendedActivity;
import com.app.selfpause.javaActivities.CreativityAffirmationActivityNew;
import com.app.selfpause.javaActivities.FavoritesActivity;
import com.app.selfpause.javaActivities.GetMorePaymentActivity;
import com.app.selfpause.javaActivities.RecyclerTouchListener;
import com.app.selfpause.javaActivities.SubscriptionActivity;
import com.app.selfpause.utilityClasses.BackgroundSoundService;
import com.app.selfpause.utilityClasses.NatureSoundService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_account;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_lib;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_record;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_sound;

/**
 * A simple {@link Fragment} subclass.
 */
public class SoundFragment extends Fragment {

    String userID;
    String mypreference = "mypref", user_id = "user_id";

    private ProgressBar progressBar;
    private RecyclerView soundScapeRV, musicRV;
    private ApiInterface apiInterface;
    private GetSoundAndScapeResponse resource;
    private LinearLayout ll_sound_search;
    private CustomBoldtextView soundScape_text, music_text;
    private List<SoundScapeModelClass> soundScapeModelClass;
    private List<MusicModelClass> musicModelClasses;

    private ImageView img_back_tool, hurt_img,img_selfpause;

    public SoundFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.sound_fragment, container, false);

        SharedPreferences preferences = getActivity().getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = preferences.getString(user_id, "");

        Log.e("id", userID);

        progressBar = view.findViewById(R.id.sound_progressBar);
        soundScapeRV = view.findViewById(R.id.soundFragment_soundScapeRV);
        musicRV = view.findViewById(R.id.soundFragment_musicRV);
        ll_sound_search = view.findViewById(R.id.ll_sound_search);
        soundScape_text = view.findViewById(R.id.soundScape_text);
        music_text = view.findViewById(R.id.music_text);
        hurt_img = view.findViewById(R.id.hurt_img);
        img_back_tool = view.findViewById(R.id.img_back_tool);
        img_selfpause = view.findViewById(R.id.img_selfpause);
        hurt_img.setVisibility(View.GONE);

        img_back_tool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                img_bottom_lib.setVisibility(View.VISIBLE);
                img_bottom_sound.setVisibility(View.GONE);
                img_bottom_record.setVisibility(View.GONE);
                img_bottom_account.setVisibility(View.GONE);

                Fragment someFragment = new LibraryFragmentNew();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.container, someFragment); // give your fragment container id in first parameter
                transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                transaction.commit();

            }
        });

        img_selfpause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LibraryFragmentNew nextFrag= new LibraryFragmentNew();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });

        hurt_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), FavoritesActivity.class);
                startActivity(intent);
            }
        });

        progressBar.setVisibility(View.VISIBLE);
        ll_sound_search.setVisibility(View.GONE);
        soundScape_text.setVisibility(View.INVISIBLE);
        music_text.setVisibility(View.INVISIBLE);

        GridLayoutManager grid_sound = new GridLayoutManager(getActivity(), 3, RecyclerView.VERTICAL, false);
        soundScapeRV.setLayoutManager(grid_sound);

        GridLayoutManager grid_music = new GridLayoutManager(getActivity(), 3, RecyclerView.VERTICAL, false);
        musicRV.setLayoutManager(grid_music);

        getData(userID);

        return view;
    }

    public void getData(String userID) {

        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);

        Call<GetSoundAndScapeResponse> call = apiInterface.getMusicList(userID);

        call.enqueue(new Callback<GetSoundAndScapeResponse>() {
            @Override
            public void onResponse(Call<GetSoundAndScapeResponse> call, final Response<GetSoundAndScapeResponse> response) {

                if (response.isSuccessful()) {
                    resource = response.body();

                    assert resource != null;
                    if (resource.getSuccess()) {

                        soundScapeModelClass = resource.getData().getSoundScopes();
                        musicModelClasses = resource.getData().getMusic();

                        progressBar.setVisibility(View.GONE);
                        ll_sound_search.setVisibility(View.GONE);
                        soundScape_text.setVisibility(View.VISIBLE);
                        music_text.setVisibility(View.VISIBLE);

                        SoundScapeAdapter soundScapeAdapter = new SoundScapeAdapter(getActivity(), resource.getData().getSoundScopes());
                        soundScapeRV.setAdapter(soundScapeAdapter);

                        final MusicAdapter musicAdapter = new MusicAdapter(getActivity(), resource.getData().getMusic());
                        musicRV.setAdapter(musicAdapter);

                        Log.e("price", resource.getData().getSoundScopes().get(1).getNatureId().toString());

                        soundScapeRV.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), soundScapeRV, new RecyclerTouchListener.ClickListener() {
                            @Override
                            public void onClick(View view, int position) {

                                String nature = soundScapeModelClass.get(position).getNatureSound();
                                int id = soundScapeModelClass.get(position).getNatureId();
                                String nature_id = String.valueOf(id);
                                String nature_name = soundScapeModelClass.get(position).getNatureName();
                                Log.e("SOUND_SCAPE_URL :  ", nature);
                                Log.e("NATURE ID : ", nature_id);
                                Log.e("NATURE NAME : ", nature_name);


//                 if (resource.getData().getSoundScopes().get(position).getLockUnlockStatus().equals(0)){
//
//                     Intent intent = new Intent(getActivity(), GetMorePaymentActivity.class);
//                     intent.putExtra("song", nature);
//                     intent.putExtra("nature_id", nature_id);
//                     intent.putExtra("nature_name", nature_name);
//                     startActivity(intent);
//                 }
//                 else if (resource.getData().getSoundScopes().get(position).getLockUnlockStatus().equals(1)){
//
//                     Intent intent = new Intent(getActivity(), CreativityAffirmationActivityNew.class);
//                     intent.putExtra("song", nature);
//                     intent.putExtra("nature_id", nature_id);
//                     intent.putExtra("nature_name", nature_name);
//                     startActivity(intent);
//                 }

                                if (resource.getData().getSoundScopes().get(position).getLockUnlockStatus().equals(0)) {
                                    Intent intent = new Intent(getActivity(), SubscriptionActivity.class);
                                    intent.putExtra("price", resource.getData().getSoundScopes().get(position).getPrice().toString());

                                    intent.putExtra("song", nature);
                                    intent.putExtra("nature_id", nature_id);
                                    intent.putExtra("nature_name", nature_name);
                                    intent.putExtra("screen", "2");
                                    startActivity(intent);
                                } else if (resource.getData().getSoundScopes().get(position).getLockUnlockStatus().equals(1)) {
                                    Intent intent = new Intent(getActivity(), CreativityAffirmationActivityNew.class);
                                    intent.putExtra("song", resource.getData().getSoundScopes().get(position).getSongs());
                                    intent.putExtra("category", resource.getData().getSoundScopes().get(position).getMusicType());
                                    intent.putExtra("song_name", resource.getData().getSoundScopes().get(position).getNatureName());
                                    intent.putExtra("type","nature");
                                    startActivity(intent);
                                }

                            }

                            @Override
                            public void onLongClick(View view, int position) {

                            }
                        }));

                        musicRV.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), musicRV, new RecyclerTouchListener.ClickListener() {
                            @Override
                            public void onClick(View view, int position) {

                                String natureMusic = musicModelClasses.get(position).getNatureSound();
                                int id = musicModelClasses.get(position).getNatureId();
                                String nature_id = String.valueOf(id);
                                String nature_name = musicModelClasses.get(position).getNatureName();
                                Log.e("NATURE ID : ", nature_id);
                                Log.e("NATURE NAME : ", nature_name);
                                Log.e("MUSIC_SOUND_URL :  ", natureMusic);


//                                if (resource.getData().getSoundScopes().get(position).getLockUnlockStatus().equals(0)){
//
//                                    Intent intent = new Intent(getActivity(), GetMorePaymentActivity.class);
//                                    intent.putExtra("song", natureMusic);
//                                    intent.putExtra("nature_id", nature_id);
//                                    intent.putExtra("nature_name", nature_name);
//                                    startActivity(intent);
//                                }
//                                else if (resource.getData().getSoundScopes().get(position).getLockUnlockStatus().equals(1)){
//
//                                    Intent intent = new Intent(getActivity(), CreativityAffirmationActivityNew.class);
//                                    intent.putExtra("song", natureMusic);
//                                    intent.putExtra("nature_id", nature_id);
//                                    intent.putExtra("nature_name", nature_name);
//                                    startActivity(intent);
//                                }

                                if (resource.getData().getMusic().get(position).getLockUnlockStatus().equals(0)) {
                                    Intent intent = new Intent(getActivity(), SubscriptionActivity.class);
                                    intent.putExtra("price", resource.getData().getMusic().get(position).getPrice().toString());
                                    intent.putExtra("song", natureMusic);
                                    intent.putExtra("nature_id", nature_id);
                                    intent.putExtra("nature_name", nature_name);
                                    startActivity(intent);
                                } else if (resource.getData().getMusic().get(position).getLockUnlockStatus().equals(1)) {
                                    Intent intent = new Intent(getActivity(), CreativityAffirmationActivityNew.class);
                                    intent.putExtra("song", resource.getData().getMusic().get(position).getSongs());
                                    intent.putExtra("category", resource.getData().getMusic().get(position).getMusicType());
                                    intent.putExtra("song_name", resource.getData().getMusic().get(position).getNatureName());
                                    intent.putExtra("type","nature");
                                    startActivity(intent);
                                }

                            }

                            @Override
                            public void onLongClick(View view, int position) {

                            }
                        }));
                    } else {
                        Toast.makeText(getActivity(), resource.getMessages(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }

                } else {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GetSoundAndScapeResponse> call, Throwable t) {

                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent m_intent = new Intent(getActivity(), BackgroundSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(getActivity(), m_intent);
//        }
//        if (isMyServiceRunning(NatureSoundService.class)) {
//            Intent m_intent = new Intent(getActivity(), NatureSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(getActivity(), m_intent);
//        }
        if (isMyServiceRunning(BackgroundSoundService.class)) {
            getActivity().stopService(new Intent(getActivity(), BackgroundSoundService.class));
        }
        if (isMyServiceRunning(NatureSoundService.class)) {
            getActivity().stopService(new Intent(getActivity(),NatureSoundService.class));
        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
