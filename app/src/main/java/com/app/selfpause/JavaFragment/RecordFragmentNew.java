package com.app.selfpause.JavaFragment;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.RecordCategoryModelClass;
import com.app.selfpause.R;
import com.app.selfpause.adapter.RecordcCategoryAdapter;
import com.app.selfpause.javaActivities.AllCatAndRecomendedActivity;
import com.app.selfpause.javaActivities.CreativityAffirmationActivityNew;
import com.app.selfpause.utilityClasses.BackgroundSoundService;
import com.app.selfpause.utilityClasses.NatureSoundService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_account;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_lib;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_record;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_sound;


public class RecordFragmentNew extends Fragment {

    String userID;
    String mypreference = "mypref", user_id = "user_id";
    RecyclerView recyclerView;
    CustomBoldtextView playall;
    RecordCategoryModelClass resource;
    ImageView img_tool_bar_three_back,img_tool_bar_three_selfpause;

    public RecordFragmentNew() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.my_recording_fragment, container, false);

        SharedPreferences preferences = getActivity().getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = preferences.getString(user_id, "");

        recyclerView = view.findViewById(R.id.recording_categoryRecycler);
        playall = view.findViewById(R.id.recording_playall);
        img_tool_bar_three_back = view.findViewById(R.id.img_tool_bar_three_back);
        img_tool_bar_three_selfpause = view.findViewById(R.id.img_tool_bar_three_selfpause);

        img_tool_bar_three_selfpause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LibraryFragmentNew nextFrag= new LibraryFragmentNew();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });

        img_tool_bar_three_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_bottom_lib.setVisibility(View.VISIBLE);
                img_bottom_sound.setVisibility(View.GONE);
                img_bottom_record.setVisibility(View.GONE);
                img_bottom_account.setVisibility(View.GONE);

                Fragment someFragment = new LibraryFragmentNew();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.container, someFragment); // give your fragment container id in first parameter
                transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                transaction.commit();

            }
        });

        playall.setOnClickListener(v -> {

            if (resource != null) {
                if (resource.getData().getPlayall().size() == 0){
                    Toast.makeText(getActivity(), "No Recordings Found", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(getActivity(), CreativityAffirmationActivityNew.class);
//                intent.putExtra("demo","https://clientstagingdev.com/meditation/public/voice/1586425636.mp3");
                    intent.putExtra("myRecording", true);
                    intent.putStringArrayListExtra("playlist", (ArrayList<String>) resource.getData().getPlayall());
                    startActivity(intent);
                }
            } else {
                Toast.makeText(getActivity(), "Data is not Loaded yet!", Toast.LENGTH_SHORT).show();
            }
        });

        LinearLayoutManager llManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(llManager);

        Log.e("id", userID);

        getData(userID);

        return view;
    }

    public void getData(String user_id) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<RecordCategoryModelClass> call = apiInterface.getRecorCategory(user_id);
        call.enqueue(new Callback<RecordCategoryModelClass>() {
            @Override
            public void onResponse(Call<RecordCategoryModelClass> call, Response<RecordCategoryModelClass> response) {
                if (response.isSuccessful()) {
                    resource = response.body();
                    if (resource.getSuccess()) {

                        RecordcCategoryAdapter adapter = new RecordcCategoryAdapter(getActivity(), resource.getData().getCategory());
                        recyclerView.setAdapter(adapter);

                    } else {
                        Toast.makeText(getActivity(), resource.getMessages(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RecordCategoryModelClass> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getData(userID);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent m_intent = new Intent(getActivity(), BackgroundSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(getActivity(), m_intent);
//        }
//        if (isMyServiceRunning(NatureSoundService.class)) {
//            Intent m_intent = new Intent(getActivity(), NatureSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(getActivity(), m_intent);
//        }
        if (isMyServiceRunning(BackgroundSoundService.class)) {
            getActivity().stopService(new Intent(getActivity(), BackgroundSoundService.class));
        }
        if (isMyServiceRunning(NatureSoundService.class)) {
            getActivity().stopService(new Intent(getActivity(),NatureSoundService.class));
        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
