package com.app.selfpause.JavaFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.GetProfileResponse;
import com.app.selfpause.R;
import com.app.selfpause.javaActivities.SettingActivity;
import com.app.selfpause.javaActivities.SubscriptionActivity;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    private CircleImageView userProfileIV;
    private CustomBoldtextView userNameTV, txt_upgrade, txt_email, account_invitefriends;
    private LinearLayout ll_setting;
    CallbackManager callbackManager;
    LoginManager loginManager;
    //    String  mypreference = "mypref",user_name="name", img="profile_photo",email="email";
    String userID;
    String mypreference = "mypref", user_id = "user_id";
    ApiInterface apiInterface;
    GetProfileResponse resource;
    private LinearLayout progressLL, allInfoLL;
    ImageView payment_premium,accountFragment_selfpause;
    private String newEmail;


    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.account_three_fragment, container, false);

        userProfileIV = view.findViewById(R.id.accountFragment_userProfileIV);
        userNameTV = view.findViewById(R.id.accountFragment_userNameTV);
        ll_setting = view.findViewById(R.id.ll_setting);
        txt_email = view.findViewById(R.id.accountThree_txt_email);
        payment_premium = view.findViewById(R.id.payment_premium);
        account_invitefriends = view.findViewById(R.id.account_invitefriends);
        accountFragment_selfpause = view.findViewById(R.id.accountFragment_selfpause);

        SharedPreferences pref = getActivity().getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = pref.getString(user_id, "");

        retrofitGetProfileData(userID);


        txt_upgrade = view.findViewById(R.id.txt_upgrade);
        progressLL = view.findViewById(R.id.accountFragment_progressLL);
        allInfoLL = view.findViewById(R.id.accountFragment_AllDetailsLL);

        progressLL.setVisibility(View.VISIBLE);

        payment_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), SubscriptionActivity.class);
////                intent.putExtra("colorcode", "1");
//                startActivity(intent);
                if (resource.getData().getIsSubscribed().equals("0")) {
                    Intent intent = new Intent(getActivity(), SubscriptionActivity.class);
//                intent.putExtra("colorcode", "1");
                    startActivity(intent);
                }else if (resource.getData().getIsSubscribed().equals("1")){
                    Toast.makeText(getActivity(), "You already have Premium", Toast.LENGTH_SHORT).show();

                }
            }
        });

        accountFragment_selfpause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LibraryFragmentNew nextFrag= new LibraryFragmentNew();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });


        account_invitefriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareApp(newEmail);
            }
        });

        txt_upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Fragment someFragment = new PromoFragment();
//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.container, someFragment ); // give your fragment container id in first parameter
//                transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
//                transaction.commit();
                if (resource.getData().getIsSubscribed().equals("0")) {
                    Intent intent = new Intent(getActivity(), SubscriptionActivity.class);
//                intent.putExtra("colorcode", "1");
                    startActivity(intent);
                }else if (resource.getData().getIsSubscribed().equals("1")){
                    Toast.makeText(getActivity(), "You have already Subscribed", Toast.LENGTH_SHORT).show();
                }

            }
        });

        ll_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), SettingActivity.class));

            }
        });

        return view;

    }

    public void retrofitGetProfileData(final String userID) {
        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);

        Call<GetProfileResponse> call = apiInterface.getProfile(userID);

        call.enqueue(new Callback<GetProfileResponse>() {
            @Override
            public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                if (response.isSuccessful()) {
                    resource = response.body();
                    assert resource != null;
                    Log.e("success", resource.getMessages());
                    if (resource.getSuccess()) {

                        progressLL.setVisibility(View.GONE);
                        allInfoLL.setVisibility(View.VISIBLE);

                        if (resource.getData().getIsSubscribed().equals("1")){
                            txt_upgrade.setText("You have Premium");
                        }
                        userNameTV.setText(resource.getData().getFirstName());
                        newEmail = resource.getData().getEmail();
                        txt_email.setText(resource.getData().getEmail());
                        if (!resource.getData().getProfile().equals("")) {
                            Picasso.get()
                                    .load(resource.getData().getProfile())
                                    .into(userProfileIV);
//                            Log.e("assss",resource.getData().getProfile()+"assa");
                        }
                    } else {
                        Toast.makeText(getActivity(), resource.getMessages(), Toast.LENGTH_SHORT).show();
                        progressLL.setVisibility(View.GONE);
                        allInfoLL.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    progressLL.setVisibility(View.GONE);
                    allInfoLL.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetProfileResponse> call, Throwable t) {
                Log.e("Failure Response++++", t.getMessage());
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                progressLL.setVisibility(View.GONE);
                allInfoLL.setVisibility(View.VISIBLE);
            }
        });

    }

    public void shareApp(String email) {

        String body = "Hi,\n \n Download Selfpause App Now! This is my Email: " + email + "."
                + " \n \n its available on,"

                + "\n\n For Android users: https://play.google.com/store/apps/details?id=com.app.selfpause"

                + "\n\n For i-phone users: https://apps.apple.com/us/app/selfpause/id1518538414";

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(Intent.EXTRA_TEXT, body);

        startActivityForResult(Intent.createChooser(sharingIntent, "Share via"), 999);

    }

}
