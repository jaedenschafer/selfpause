package com.app.selfpause.JavaFragment;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.selfpause.Api.ApiInterface;
import com.app.selfpause.Api.RetrofitClientInstance;
import com.app.selfpause.Custom_Widgets.CustomBoldEditText;
import com.app.selfpause.Custom_Widgets.CustomBoldtextView;
import com.app.selfpause.ModelClasses.CategoryData;
import com.app.selfpause.ModelClasses.GetHomeResponse;
import com.app.selfpause.ModelClasses.InterestedData;
import com.app.selfpause.ModelClasses.RandomData;
import com.app.selfpause.R;
import com.app.selfpause.adapter.CategoryAdapter;
import com.app.selfpause.adapter.InterestAdapter;
import com.app.selfpause.adapter.NatureAdapter;
import com.app.selfpause.javaActivities.AllCatAndRecomendedActivity;
import com.app.selfpause.javaActivities.CategoriesActivities;
import com.app.selfpause.javaActivities.FavoritesActivity;
import com.app.selfpause.utilityClasses.BackgroundSoundService;
import com.app.selfpause.utilityClasses.NatureSoundService;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_account;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_lib;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_record;
import static com.app.selfpause.javaActivities.HomeActivitynew.img_bottom_sound;

public class LibraryFragmentNew extends Fragment {

    CustomBoldtextView ll_weight_lib, txt_home_my_recording, txt_home_my_favourite, interest_title, stuff_title,
            nature_title, categories_title, interest_edit;
    LinearLayout ll_weight_lib_two, stuff_ll;
    RecyclerView interestRecyclerView, natureRecyclerView, categoryRecyclerView;
    String userID;
    String mypreference = "mypref", user_id = "user_id";
    ApiInterface apiInterface;
    GetHomeResponse resource;
    RelativeLayout progressBar;
    ImageView bigMainImage, selfpause;
    CustomBoldEditText search_lib_ET;
    CategoryAdapter categoryAdapter;
    List<InterestedData> interestedData;
    RandomData randomData;
    public static boolean update = false;

    public LibraryFragmentNew() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lib, container, false);

        SharedPreferences preferences = getActivity().getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        userID = preferences.getString(user_id, "");

        progressBar = view.findViewById(R.id.lib_frag__prog_rl);
//        ll_weight_lib = view.findViewById(R.id.ll_weight_lib);
//        ll_weight_lib_two = view.findViewById(R.id.ll_weight_lib_two);
        bigMainImage = view.findViewById(R.id.weight_lib_two_image);
        interestRecyclerView = view.findViewById(R.id.lib_interestRecyclerView);
        natureRecyclerView = view.findViewById(R.id.lib_natureRecyclerView);
        categoryRecyclerView = view.findViewById(R.id.lib_categoryRecyclerView);
        txt_home_my_recording = view.findViewById(R.id.txt_home_my_recording);
        txt_home_my_favourite = view.findViewById(R.id.txt_home_my_favourite);
        search_lib_ET = view.findViewById(R.id.search_lib_ET);
        interest_title = view.findViewById(R.id.interest_title);
        stuff_title = view.findViewById(R.id.stuff_title);
        stuff_ll = view.findViewById(R.id.stuff_ll);
        nature_title = view.findViewById(R.id.nature_title);
        categories_title = view.findViewById(R.id.categories_title);
        interest_edit = view.findViewById(R.id.interest_edit);
        selfpause = view.findViewById(R.id.lib_frag__selfpause);

        img_bottom_lib.setVisibility(View.VISIBLE);
        img_bottom_sound.setVisibility(View.GONE);
        img_bottom_record.setVisibility(View.GONE);
        img_bottom_account.setVisibility(View.GONE);

        selfpause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LibraryFragmentNew nextFrag= new LibraryFragmentNew();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });

        bigMainImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer cat = resource.getData().getRandom().getId();
                String cat_id = String.valueOf(cat);
                Log.e("CAT_ID", cat_id);

                Intent intent = new Intent(getActivity(), AllCatAndRecomendedActivity.class);
                intent.putExtra("cat_id", cat_id);
                startActivity(intent);

            }
        });

        txt_home_my_recording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                img_bottom_lib.setVisibility(View.GONE);
                img_bottom_sound.setVisibility(View.GONE);
                img_bottom_record.setVisibility(View.VISIBLE);
                img_bottom_account.setVisibility(View.GONE);

                RecordFragmentNew recordFragment = new RecordFragmentNew();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, recordFragment);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();
            }
        });

        txt_home_my_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), FavoritesActivity.class);
                startActivity(intent);
            }
        });

        interest_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CategoriesActivities.class);
                intent.putExtra("list", (Serializable) interestedData);
                startActivity(intent);
            }
        });

        progressBar.setVisibility(View.VISIBLE);

        LinearLayoutManager llManager_interest = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        interestRecyclerView.setLayoutManager(llManager_interest);

        GridLayoutManager glManager_nature = new GridLayoutManager(getActivity(), 3, RecyclerView.VERTICAL, false);
        natureRecyclerView.setLayoutManager(glManager_nature);

        LinearLayoutManager llManager_allcat = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        categoryRecyclerView.setLayoutManager(llManager_allcat);

        getHomeData(userID, "2");

//        search_lib_ET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    performSearch(userID, search_lib_ET.getText().toString());
//                    return true;
//                }
//                return false;
//            }
//        });

        search_lib_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String text = editable.toString()
                            .toLowerCase(Locale.getDefault());
                    categoryAdapter.filter(text);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (editable.toString().trim().equals("")) {
                    natureRecyclerView.setVisibility(View.VISIBLE);
                    interestRecyclerView.setVisibility(View.VISIBLE);
                    bigMainImage.setVisibility(View.VISIBLE);
                    interest_title.setVisibility(View.VISIBLE);
                    interest_edit.setVisibility(View.VISIBLE);
                    stuff_title.setVisibility(View.VISIBLE);
                    stuff_ll.setVisibility(View.VISIBLE);
                    nature_title.setVisibility(View.VISIBLE);
                    categories_title.setVisibility(View.VISIBLE);
                } else {
                    natureRecyclerView.setVisibility(View.GONE);
                    interestRecyclerView.setVisibility(View.GONE);
                    bigMainImage.setVisibility(View.GONE);
                    interest_title.setVisibility(View.GONE);
                    interest_edit.setVisibility(View.GONE);
                    stuff_title.setVisibility(View.GONE);
                    stuff_ll.setVisibility(View.GONE);
                    nature_title.setVisibility(View.GONE);
                    categories_title.setVisibility(View.GONE);
                }
//                filter(editable.toString());
//                Log.e("a",editable.toString());
            }
        });

        return view;
    }

    private void performSearch(String userID, String title) {
        ApiInterface apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<GetHomeResponse> call = apiInterface.getSearchCategory(userID, title);
        call.enqueue(new Callback<GetHomeResponse>() {
            @Override
            public void onResponse(Call<GetHomeResponse> call, Response<GetHomeResponse> response) {
                if (response.isSuccessful()) {
                    GetHomeResponse resource = response.body();
                    if (resource.getSuccess()) {

                        Integer cat = resource.getData().getCategories().get(0).getId();
                        String cat_id = String.valueOf(cat);
                        Log.e("CAT_ID", cat_id);

                        Intent intent = new Intent(getActivity(), AllCatAndRecomendedActivity.class);
                        intent.putExtra("cat_id", cat_id);
                        startActivity(intent);

                    }

                } else {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetHomeResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void filter(String charText) {
//
//        List<CategoryData> categoryData = new ArrayList<>();
//
//        for (CategoryData data : categoryData) {
//
//            if (data.getName().toLowerCase().contains(charText.toLowerCase())) {
//                categoryData.add(data);
//            }
//        }
//
////        charText = charText.toLowerCase(Locale.getDefault());
//
////        list_data.clear();
////        if (charText.length() == 0) {
////
////            list_data.addAll(mDisplayedValues);
////        } else {
////
////            for (int i = 0; i < mDisplayedValues.size(); i++) {
////
////                if (mDisplayedValues.get(i).getTitle()
////                        .toLowerCase(Locale.getDefault())
////                        .contains(charText)) {
////
////                    list_data.add(mDisplayedValues.get(i));
////
////                }
////            }
////        }
////        notifyDataSetChanged();
//
//    }

    public void getHomeData(String userID, String typeId) {
        apiInterface = RetrofitClientInstance.createService(ApiInterface.class);
        Call<GetHomeResponse> call = apiInterface.getHome(userID, typeId);
        call.enqueue(new Callback<GetHomeResponse>() {
            @Override
            public void onResponse(Call<GetHomeResponse> call, Response<GetHomeResponse> response) {
                if (response.isSuccessful()) {
                    resource = response.body();
                    assert resource != null;
                    if (resource.getSuccess()) {

                        Picasso.get().load(resource.getData().getRandom().getImage()).into(bigMainImage);

                        interestedData = resource.getData().getInterested();
                        InterestAdapter interestAdapter = new InterestAdapter(getActivity(), resource.getData().getInterested());
                        interestRecyclerView.setAdapter(interestAdapter);
                        Log.e("interest", String.valueOf(resource.getData().getInterested().size()));

                        NatureAdapter natureAdapter = new NatureAdapter(getActivity(), resource.getData().getNature());
                        natureRecyclerView.setAdapter(natureAdapter);
                        Log.e("nature", String.valueOf(resource.getData().getNature().size()));

                        categoryAdapter = new CategoryAdapter(getActivity(), resource.getData().getCategories());
                        categoryRecyclerView.setAdapter(categoryAdapter);
                        Log.e("interest", String.valueOf(resource.getData().getCategories().size()));

                        progressBar.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GetHomeResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        if (isMyServiceRunning(BackgroundSoundService.class)) {
//            Intent m_intent = new Intent(getActivity(), BackgroundSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(getActivity(), m_intent);
//        }
//        if (isMyServiceRunning(NatureSoundService.class)) {
//            Intent m_intent = new Intent(getActivity(), NatureSoundService.class);
//            m_intent.putExtra("player", "Pause");
//            ContextCompat.startForegroundService(getActivity(), m_intent);
//        }
        if (isMyServiceRunning(BackgroundSoundService.class)) {
            getActivity().stopService(new Intent(getActivity(), BackgroundSoundService.class));
        }
        if (isMyServiceRunning(NatureSoundService.class)) {
            getActivity().stopService(new Intent(getActivity(),NatureSoundService.class));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
