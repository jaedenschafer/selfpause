package com.app.selfpause.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetFavouriteSessionData {
    @SerializedName("favourite")
    @Expose
    private List<FavouriteData> favourite = null;

    public List<FavouriteData> getFavourite() {
        return favourite;
    }

    public void setFavourite(List<FavouriteData> favourite) {
        this.favourite = favourite;
    }
}
