package com.app.selfpause.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavouriteData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cat_id")
    @Expose
    private Integer catId;
    @SerializedName("artist_id")
    @Expose
    private Integer artistId;
    @SerializedName("affirmation_songs")
    @Expose
    private String affirmationSongs;
    @SerializedName("affirmation_images")
    @Expose
    private String affirmationImages;
    @SerializedName("affirmation_duration")
    @Expose
    private String affirmationDuration;
    @SerializedName("affirmation_title")
    @Expose
    private String affirmationTitle;
    @SerializedName("affirmation_subtitle")
    @Expose
    private String affirmationSubtitle;
    @SerializedName("affirmation_discription")
    @Expose
    private String affirmationDiscription;
    @SerializedName("affirmation_favrites")
    @Expose
    private Object affirmationFavrites;
    @SerializedName("songs_type")
    @Expose
    private Integer songsType;
    @SerializedName("lock_images")
    @Expose
    private Object lockImages;
    @SerializedName("unlock_images")
    @Expose
    private Object unlockImages;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("free_premium")
    @Expose
    private Integer freePremium;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("favourite_id")
    @Expose
    private Integer favouriteId;
    @SerializedName("favourite_session_id")
    @Expose
    private Integer favouriteSessionId;
    @SerializedName("favourite_userid")
    @Expose
    private Integer favouriteUserid;
    @SerializedName("favourite_status")
    @Expose
    private Integer favouriteStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getArtistId() {
        return artistId;
    }

    public void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }

    public String getAffirmationSongs() {
        return affirmationSongs;
    }

    public void setAffirmationSongs(String affirmationSongs) {
        this.affirmationSongs = affirmationSongs;
    }

    public String getAffirmationImages() {
        return affirmationImages;
    }

    public void setAffirmationImages(String affirmationImages) {
        this.affirmationImages = affirmationImages;
    }

    public String getAffirmationDuration() {
        return affirmationDuration;
    }

    public void setAffirmationDuration(String affirmationDuration) {
        this.affirmationDuration = affirmationDuration;
    }

    public String getAffirmationTitle() {
        return affirmationTitle;
    }

    public void setAffirmationTitle(String affirmationTitle) {
        this.affirmationTitle = affirmationTitle;
    }

    public String getAffirmationSubtitle() {
        return affirmationSubtitle;
    }

    public void setAffirmationSubtitle(String affirmationSubtitle) {
        this.affirmationSubtitle = affirmationSubtitle;
    }

    public String getAffirmationDiscription() {
        return affirmationDiscription;
    }

    public void setAffirmationDiscription(String affirmationDiscription) {
        this.affirmationDiscription = affirmationDiscription;
    }

    public Object getAffirmationFavrites() {
        return affirmationFavrites;
    }

    public void setAffirmationFavrites(Object affirmationFavrites) {
        this.affirmationFavrites = affirmationFavrites;
    }

    public Integer getSongsType() {
        return songsType;
    }

    public void setSongsType(Integer songsType) {
        this.songsType = songsType;
    }

    public Object getLockImages() {
        return lockImages;
    }

    public void setLockImages(Object lockImages) {
        this.lockImages = lockImages;
    }

    public Object getUnlockImages() {
        return unlockImages;
    }

    public void setUnlockImages(Object unlockImages) {
        this.unlockImages = unlockImages;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFreePremium() {
        return freePremium;
    }

    public void setFreePremium(Integer freePremium) {
        this.freePremium = freePremium;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getFavouriteId() {
        return favouriteId;
    }

    public void setFavouriteId(Integer favouriteId) {
        this.favouriteId = favouriteId;
    }

    public Integer getFavouriteSessionId() {
        return favouriteSessionId;
    }

    public void setFavouriteSessionId(Integer favouriteSessionId) {
        this.favouriteSessionId = favouriteSessionId;
    }

    public Integer getFavouriteUserid() {
        return favouriteUserid;
    }

    public void setFavouriteUserid(Integer favouriteUserid) {
        this.favouriteUserid = favouriteUserid;
    }

    public Integer getFavouriteStatus() {
        return favouriteStatus;
    }

    public void setFavouriteStatus(Integer favouriteStatus) {
        this.favouriteStatus = favouriteStatus;
    }
}
