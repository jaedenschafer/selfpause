package com.app.selfpause.ModelClasses;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MusicPlayerData {

    @SerializedName("nature")
    @Expose
    private List<MusicPlayerNature> nature = null;
    @SerializedName("getVoice")
    @Expose
    private List<MusicPlayerGetVoice> getVoice = null;

    public List<MusicPlayerNature> getNature() {
        return nature;
    }

    public void setNature(List<MusicPlayerNature> nature) {
        this.nature = nature;
    }

    public List<MusicPlayerGetVoice> getGetVoice() {
        return getVoice;
    }

    public void setGetVoice(List<MusicPlayerGetVoice> getVoice) {
        this.getVoice = getVoice;
    }
}
