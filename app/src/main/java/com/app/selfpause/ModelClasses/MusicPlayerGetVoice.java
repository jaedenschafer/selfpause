package com.app.selfpause.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MusicPlayerGetVoice {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("voices")
    @Expose
    private String voices;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("lockUnlockStatus")
    @Expose
    private Integer lockUnlockStatus;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("songs")
    @Expose
    private String songs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getVoices() {
        return voices;
    }

    public void setVoices(String voices) {
        this.voices = voices;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getLockUnlockStatus() {
        return lockUnlockStatus;
    }

    public void setLockUnlockStatus(Integer lockUnlockStatus) {
        this.lockUnlockStatus = lockUnlockStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSongs() {
        return songs;
    }

    public void setSongs(String songs) {
        this.songs = songs;
    }
}
