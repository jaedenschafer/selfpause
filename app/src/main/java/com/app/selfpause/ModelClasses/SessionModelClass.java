package com.app.selfpause.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionModelClass {

    @SerializedName("nature_id")
    @Expose
    private Integer natureId;
    @SerializedName("nature_name")
    @Expose
    private String natureName;
    @SerializedName("nature_image")
    @Expose
    private String natureImage;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("lock_images")
    @Expose
    private Object lockImages;
    @SerializedName("nature_sound")
    @Expose
    private String natureSound;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("music_type")
    @Expose
    private Integer musicType;
    @SerializedName("nature_date")
    @Expose
    private String natureDate;
    @SerializedName("nature_status")
    @Expose
    private Integer natureStatus;
    @SerializedName("free_premium")
    @Expose
    private Integer freePremium;
    @SerializedName("affirmation_songs")
    @Expose
    private String affirmationSongs;
    @SerializedName("affirmation_images")
    @Expose
    private String affirmationImages;
    @SerializedName("affirmation_title")
    @Expose
    private String affirmationTitle;
    @SerializedName("favourite")
    @Expose
    private Integer favourite;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("lockUnlockStatus")
    @Expose
    private Integer lockUnlockStatus;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("songs")
    @Expose
    private String songs;

    public Integer getNatureId() {
        return natureId;
    }

    public void setNatureId(Integer natureId) {
        this.natureId = natureId;
    }

    public String getNatureName() {
        return natureName;
    }

    public void setNatureName(String natureName) {
        this.natureName = natureName;
    }

    public String getNatureImage() {
        return natureImage;
    }

    public void setNatureImage(String natureImage) {
        this.natureImage = natureImage;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Object getLockImages() {
        return lockImages;
    }

    public void setLockImages(Object lockImages) {
        this.lockImages = lockImages;
    }

    public String getNatureSound() {
        return natureSound;
    }

    public void setNatureSound(String natureSound) {
        this.natureSound = natureSound;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getMusicType() {
        return musicType;
    }

    public void setMusicType(Integer musicType) {
        this.musicType = musicType;
    }

    public String getNatureDate() {
        return natureDate;
    }

    public void setNatureDate(String natureDate) {
        this.natureDate = natureDate;
    }

    public Integer getNatureStatus() {
        return natureStatus;
    }

    public void setNatureStatus(Integer natureStatus) {
        this.natureStatus = natureStatus;
    }

    public Integer getFreePremium() {
        return freePremium;
    }

    public void setFreePremium(Integer freePremium) {
        this.freePremium = freePremium;
    }

    public String getAffirmationSongs() {
        return affirmationSongs;
    }

    public void setAffirmationSongs(String affirmationSongs) {
        this.affirmationSongs = affirmationSongs;
    }

    public String getAffirmationImages() {
        return affirmationImages;
    }

    public void setAffirmationImages(String affirmationImages) {
        this.affirmationImages = affirmationImages;
    }

    public String getAffirmationTitle() {
        return affirmationTitle;
    }

    public void setAffirmationTitle(String affirmationTitle) {
        this.affirmationTitle = affirmationTitle;
    }

    public Integer getFavourite() {
        return favourite;
    }

    public void setFavourite(Integer favourite) {
        this.favourite = favourite;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getLockUnlockStatus() {
        return lockUnlockStatus;
    }

    public void setLockUnlockStatus(Integer lockUnlockStatus) {
        this.lockUnlockStatus = lockUnlockStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSongs() {
        return songs;
    }

    public void setSongs(String songs) {
        this.songs = songs;
    }

    //

//    @SerializedName("nature_id")
//    @Expose
//    private Integer natureId;
//    @SerializedName("nature_name")
//    @Expose
//    private String natureName;
//    @SerializedName("nature_image")
//    @Expose
//    private String natureImage;
//    @SerializedName("icon")
//    @Expose
//    private String icon;
//    @SerializedName("price")
//    @Expose
//    private Integer price;
//    @SerializedName("lock_images")
//    @Expose
//    private String lockImages;
//    @SerializedName("nature_sound")
//    @Expose
//    private String natureSound;
//    @SerializedName("duration")
//    @Expose
//    private String duration;
//    @SerializedName("music_type")
//    @Expose
//    private String musicType;
//    @SerializedName("nature_date")
//    @Expose
//    private String natureDate;
//    @SerializedName("nature_status")
//    @Expose
//    private Integer natureStatus;
//    @SerializedName("free_premium")
//    @Expose
//    private Integer freePremium;
//    @SerializedName("images")
//    @Expose
//    private String images;
//    @SerializedName("lockUnlockStatus")
//    @Expose
//    private Integer lockUnlockStatus;
//    @SerializedName("name")
//    @Expose
//    private String name;
//    @SerializedName("songs")
//    @Expose
//    private String songs;
//
//    public Integer getNatureId() {
//        return natureId;
//    }
//
//    public void setNatureId(Integer natureId) {
//        this.natureId = natureId;
//    }
//
//    public String getNatureName() {
//        return natureName;
//    }
//
//    public void setNatureName(String natureName) {
//        this.natureName = natureName;
//    }
//
//    public String getNatureImage() {
//        return natureImage;
//    }
//
//    public void setNatureImage(String natureImage) {
//        this.natureImage = natureImage;
//    }
//
//    public String getIcon() {
//        return icon;
//    }
//
//    public void setIcon(String icon) {
//        this.icon = icon;
//    }
//
//    public Integer getPrice() {
//        return price;
//    }
//
//    public void setPrice(Integer price) {
//        this.price = price;
//    }
//
//    public String getLockImages() {
//        return lockImages;
//    }
//
//    public void setLockImages(String lockImages) {
//        this.lockImages = lockImages;
//    }
//
//    public String getNatureSound() {
//        return natureSound;
//    }
//
//    public void setNatureSound(String natureSound) {
//        this.natureSound = natureSound;
//    }
//
//    public String getDuration() {
//        return duration;
//    }
//
//    public void setDuration(String duration) {
//        this.duration = duration;
//    }
//
//    public String getMusicType() {
//        return musicType;
//    }
//
//    public void setMusicType(String musicType) {
//        this.musicType = musicType;
//    }
//
//    public String getNatureDate() {
//        return natureDate;
//    }
//
//    public void setNatureDate(String natureDate) {
//        this.natureDate = natureDate;
//    }
//
//    public Integer getNatureStatus() {
//        return natureStatus;
//    }
//
//    public void setNatureStatus(Integer natureStatus) {
//        this.natureStatus = natureStatus;
//    }
//
//    public Integer getFreePremium() {
//        return freePremium;
//    }
//
//    public void setFreePremium(Integer freePremium) {
//        this.freePremium = freePremium;
//    }
//
//    public String getImages() {
//        return images;
//    }
//
//    public void setImages(String images) {
//        this.images = images;
//    }
//
//    public Integer getLockUnlockStatus() {
//        return lockUnlockStatus;
//    }
//
//    public void setLockUnlockStatus(Integer lockUnlockStatus) {
//        this.lockUnlockStatus = lockUnlockStatus;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getSongs() {
//        return songs;
//    }
//
//    public void setSongs(String songs) {
//        this.songs = songs;
//    }
    
}
