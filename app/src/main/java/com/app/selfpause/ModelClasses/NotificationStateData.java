package com.app.selfpause.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationStateData {
    @SerializedName("status")
    @Expose
    private String on;

    public String getOn() {
        return on;
    }

    public void setOn(String on) {
        this.on = on;
    }
}
