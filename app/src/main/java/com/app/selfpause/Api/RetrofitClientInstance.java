package com.app.selfpause.Api;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

//    private static Retrofit.Builder retrofit;
//     private static final String BASE_URL = "http://meditation.customer-devreview.com/api/";
    private static final String BASE_URL = "https://mayorkoch.com/api/";

    private static OkHttpClient.Builder client = new OkHttpClient.Builder()
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS);
//    https://clientstagingdev.com/meditation/api/

//    public static Retrofit.Builder getRetrofitInstance() {
//        if (retrofit == null) {
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create());
//        }
//        return retrofit;
//    }

    private static Retrofit.Builder retrofit =
            new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory
                    (GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null);

    }

    public static <S> S createService(Class<S> serviceClass, final String authToken) {
        if (authToken != null) {
            client.interceptors().add(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("authenticate-token", authToken)
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient clienta = client.build();
        client.addInterceptor(interceptor);
        Retrofit retrofita = retrofit.client(clienta).build();
        return retrofita.create(serviceClass);
    }

}
