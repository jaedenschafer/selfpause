package com.app.selfpause.Api;

import com.app.selfpause.ModelClasses.AllCatModelClasses.GetCategoryAndRecomendedModelClass;
import com.app.selfpause.ModelClasses.CheckVoiceResponse;
import com.app.selfpause.ModelClasses.DeleteAffirmationResponse;
import com.app.selfpause.ModelClasses.EditVoiceResponse;
import com.app.selfpause.ModelClasses.FavoriteModelClass.GetFavoritesModelClass;
import com.app.selfpause.ModelClasses.FavoriteModelClass.GetFavouriteData;
import com.app.selfpause.ModelClasses.ForgetPasswordModel;
import com.app.selfpause.ModelClasses.GetAffirmation;
import com.app.selfpause.ModelClasses.GetCategoriesModelClass;
import com.app.selfpause.ModelClasses.GetEditProfileResponse;
import com.app.selfpause.ModelClasses.GetFavouriteSession;
import com.app.selfpause.ModelClasses.GetHomeResponse;
import com.app.selfpause.ModelClasses.GetProfileResponse;
import com.app.selfpause.ModelClasses.GetResponsePricyAndPolicy;
import com.app.selfpause.ModelClasses.GetResponseSetVoice;
import com.app.selfpause.ModelClasses.GetResponseSubscription;
import com.app.selfpause.ModelClasses.GetResponseTermsAndCondition;
import com.app.selfpause.ModelClasses.GetSocialLoginResponse;
import com.app.selfpause.ModelClasses.GetSupportResponse;
import com.app.selfpause.ModelClasses.GetUserVoiceData;
import com.app.selfpause.ModelClasses.GetUserVoiceResponse;
import com.app.selfpause.ModelClasses.GetVoiceResponse;
import com.app.selfpause.ModelClasses.LoginModelClass;
import com.app.selfpause.ModelClasses.LoginSendData;
import com.app.selfpause.ModelClasses.LogoutModelClass;
import com.app.selfpause.ModelClasses.MusicPlayerResponse;
import com.app.selfpause.ModelClasses.NotificationResponse;
import com.app.selfpause.ModelClasses.NotificationStateResponse;
import com.app.selfpause.ModelClasses.PostAffirmation;
import com.app.selfpause.ModelClasses.RecordCategoryModelClass;
import com.app.selfpause.ModelClasses.SetCategoriesModelClass;
import com.app.selfpause.ModelClasses.SetCategoryResponse;
import com.app.selfpause.ModelClasses.SetVoiceModelClass;
import com.app.selfpause.ModelClasses.SignupModelClass;
import com.app.selfpause.ModelClasses.SignupSendData;
import com.app.selfpause.ModelClasses.SoundModel.GetSoundAndScapeResponse;
import com.app.selfpause.ModelClasses.SubscriptionModelClass;
import com.app.selfpause.ModelClasses.UserPayModel.GetUserPayModelClass;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("auth/register")
    Call<SignupModelClass> signup(@Body SignupSendData sendData);

    @POST("auth/login")
    Call<LoginModelClass> login(@Body LoginSendData loginSendData);

    @POST("auth/getProfile")
    Call<GetProfileResponse> getProfile(@Query("user_id") String userId);

    @Multipart
    @POST("auth/editProfile")
    Call<GetEditProfileResponse> editProfile(@Part("user_id") RequestBody userId, @Part("first_name") RequestBody firstName,
                                             @Part("last_name") RequestBody lastName, @Part("old_password") RequestBody password,
                                             @Part("new_password") RequestBody new_password, @Part MultipartBody.Part part);


    @POST("auth/socailLogin")
    Call<GetSocialLoginResponse> getSocialLogin(@Query("social_id") String socialId, @Query("social_type") String socialType,
                                                @Query("email") String email, @Query("profile") String profile,
                                                @Query("first_name") String name, @Query("device_type") String deviceType,
                                                @Query("device_token") String deviceToken);

    @POST("auth/logout")
    Call<LogoutModelClass> getLogout(@Query("user_id") String userid);

    @POST("collections/setVoice")
    Call<GetResponseSetVoice> setVoice(@Body SetVoiceModelClass setVoiceModelClass);

    @POST("collections/getVoice")
    Call<GetVoiceResponse> getVoiceResponse();

    @GET("auth/termsCondtions")
    Call<GetResponseTermsAndCondition> termsAndCondition();

    @GET("auth/privacyPolicy")
    Call<GetResponsePricyAndPolicy> privacyAndPolicy();

    @POST("auth/checkSubscription")
    Call<GetResponseSubscription> subscription(@Body SubscriptionModelClass subscriptionModelClass);

    @POST("auth/forgotPassword")
    Call<ForgetPasswordModel> forgetPassword(@Query("email") String email);

    //    @POST("collections/getContentsInfo")
    @POST("collections/getCategoryes")
    Call<GetCategoriesModelClass> getCategory(@Query("user_id") String userId,
                                              @Query("type_id") String typeId);

//    @POST("collections/collectCategory")
//    Call<SetCategoriesModelClass> setCategory(@Query("user_id") String userId,
//                                              @Query("category_id") List<String> catagoryID);

    @POST("collections/collectCategory")
    Call<SetCategoriesModelClass> setCategory(@Body SetCategoryResponse setCategoryResponse);

    @POST("collections/randomCategory")
    Call<GetHomeResponse> getHome(@Query("user_id") String userId, @Query("type_id") String typeId);

    @POST("collections/support")
    Call<GetSupportResponse> sendQuery(@Query("user_id") String userId, @Query("suppert_subject") String subject, @Query("support_message") String message);

    @POST("collections/music")
    Call<GetSoundAndScapeResponse> getMusicList(@Query("user_id") String userId);

    @POST("payment/userPayment")
    Call<GetUserPayModelClass> getUserPayData(@Query("users_id") String user_id, @Query("payment_type") String payment_type,
                                              @Query("payment_id") String payment_id, @Query("payment_amount") String payment_amount,
                                              @Query("payment_date") String payment_date, @Query("payment_plan_id") String plan_id,
                                              @Query("payment_plan_name") String plan_name, @Query("currency_code") String currency_code,
                                              @Query("short_description") String short_desc, @Query("intent") String intent, @Query("package_type") Integer package_type,
                                              @Query("screen_name") String screen, @Query("subscription") String subscription);

    @POST("collections/affirmationCategoies")
    Call<GetCategoryAndRecomendedModelClass> getCatAndRecomended(@Query("user_id") String user_Id,
                                                                 @Query("cat_id") String cat_ID);

    @POST("collections/mySongsList")
    Call<GetAffirmation> requestAffirmation(@Query("user_id") String userId, @Query("cat_id") String categoryId);

    @Multipart
    @POST("collections/postMySongs")
    Call<PostAffirmation> postAffirmation(@Part("user_id") RequestBody userId, @Part("songs_title") RequestBody songTitle,
                                          @Part("cat_id") RequestBody categoryId, @Part("songs_id") RequestBody songId,
                                          @Part("favrite") RequestBody favourite, @Part MultipartBody.Part part);

//    @Multipart
//    @POST("collections/postMySongs")
//    Call<PostAffirmation> postAffirmation(@Part("user_id") RequestBody userId, @Part("songs_title") RequestBody songTitle,
//                                          @Part("cat_id") RequestBody categoryId, @Part("songs_id") RequestBody songId,
//                                          @Part("favrite") RequestBody favourite, @Part MultipartBody.Part part);

    @POST("collections/myfavoritesongs")
    Call<GetFavoritesModelClass> getFavorites(@Query("user_id") String user_id);

    @POST("collections/musicPlayer")
    Call<MusicPlayerResponse> getMusicResponse(@Query("user_id") String user_id,@Query("session_id")String session_id);

    @POST("collections/myRecording ")
    Call<RecordCategoryModelClass> getRecorCategory(@Query("user_id") String user_id);

    @POST("collections/searchCategory")
    Call<GetHomeResponse> getSearchCategory(@Query("user_id") String userId, @Query("title") String title);

    @POST("collections/notificationlist")
    Call<NotificationResponse> getNotification(@Query("user_id") String userId);

    @POST("collections/is_notification")
    Call<NotificationStateResponse> getNotificationState(@Query("user_id") String userId,@Query("status") String state);

    @POST("collections/deleteRecording")
    Call<DeleteAffirmationResponse> deleteAffirmation(@Query("user_id") String userId, @Query("recording_id") String recordingId);

    @POST("collections/getUserVoiceList")
    Call<GetUserVoiceResponse> getUserVoice(@Query("user_id") String userId);

    @POST("collections/editVoice")
    Call<EditVoiceResponse> setEditVoice(@Query("user_id") String userId, @Query("voice_id") String voiceId,
                                         @Query("time") String time, @Query("status") String status);

    @POST("collections/checkVoice")
    Call<CheckVoiceResponse> checkVoice(@Query("user_id")String userId);

    @POST("collections/favouriteSession")
    Call<GetFavouriteSession> getFavouriteSession(@Query("user_id") String userId, @Query("session_id") String state);

}
